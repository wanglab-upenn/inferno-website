<!DOCTYPE html>
<html lang="en-US">

<head>
  <meta charset="utf-8">
  <title>INFERNO - INFERring the molecular mechanisms of NOncoding genetic variants</title>
  <!-- <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="Description" lang="en" content="INFERNO - INFERring the molecular mechanisms of NOncoding genetic variants">
  <meta name="author" content="Alexandre Amlie-Wolf, Ph.D. Student, Genomics
			 and Computational Biology, University of Pennsylvania">
  <meta name="robots" content="index, follow">
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/manifest.json">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="theme-color" content="#ffffff">

  <!-- Stylesheets, inlcuding custom fonts, go here -->
  <link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css" type="text/css" media="all">
  <link rel="stylesheet" href="style.css" type="text/css" media="all">

  <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js">></script>
  <!-- Scripts for accessible mobile-first navigation -->
  <script type="text/javascript" src="js/navigation.js"></script>
  <script type="text/javascript" src="js/custom.js"></script>
</head>

<body>
  <a class="skip-link screen-reader-text" href="#content">Skip to content</a>

  <header class="masthead">
    <div class="site-branding">

        <figure class="logo"><img src="img/inferno_logo.svg" alt="Inferno"></figure>
        <p class="site-title">INFERring the molecular mechanisms of NOncoding genetic variants</p>

    </div><!-- .site-title -->

  </header><!-- .masthead -->
<div class="content output">
