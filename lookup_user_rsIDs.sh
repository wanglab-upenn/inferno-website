#!/bin/bash
#set -x
## lookup_user_rsIDs.sh
## takes in a string of rsIDs supplied by an INFERNO user and generates a pipeline input file

#kgSNPs=/home/alexamlie/data/INFERNO/input/all_1kg_EUR_phase1_v3_snps.txt
kgSNPs=/mnt/data/INFERNO/input/all_1kg_phase3_snps.hg19.txt.gz

OUTF=$1
shift

RSIDS="$@"
NUM_SNPS="$#"
GREP_STRING=`echo $RSIDS | sed 's/ /\|/g'`
zgrep -w -i -E "$GREP_STRING" "${kgSNPs}" > $OUTF

NUM_FOUND=`cat $OUTF | wc -l`
if [[ $NUM_FOUND -ne $NUM_SNPS ]]; then
    echo "Did not find all input rsIDs. Out of $NUM_SNPS input, found $NUM_FOUND in data."
else
    echo "Found all $NUM_SNPS input rsIDs!"
fi

