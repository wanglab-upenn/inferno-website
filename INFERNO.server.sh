#!/bin/bash

## INFERNO.sh
## Alex Amlie-Wolf
## wrapper script for the main INFERNO python function so that you can run it in bsub

#export PATH=$PATH:/home/alexamlie/bin/

## just feed all the arguments to the python script
python -u /var/www/code/INFERNO/INFERNO.py $*
