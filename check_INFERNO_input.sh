#!/bin/bash

## check_INFERNO_input.sh
## takes in a file and checks that it matches the INFERNO format

INFILE=$1

## first get rid of any windows / mac returns
sed 's/\r//g' ${INFILE} > ${INFILE}.parsed

## check the column numbers
COL_NUMS=`awk -F$'\t' '{i=0; for(j=1; j<=NF; j++) {if($j != "") {i++}} {print i}}' ${INFILE}.parsed | sort -u`
if [ "${COL_NUMS}" != "4" ]; then
    echo "Incorrect number of columns. Every line should have 4, input file includes lines with the following number(s) of non-empty columns:"
    echo ${COL_NUMS}
    rm ${INFILE}.parsed
    exit 1
fi

## check that the positions are all numeric
NUM_POS_COLS=`awk -F$'\t' '$4 ~/^[0-9]+$/' ${INFILE}.parsed | wc -l`
NUM_TOT_LINES=`cut -f4 ${INFILE}.parsed | wc -l`
if [ "${NUM_POS_COLS}" -ne "${NUM_TOT_LINES}" ]; then
    echo "${NUM_POS_COLS} numeric position entries found out of ${NUM_TOT_LINES}. All must be numeric values!"
    rm ${INFILE}.parsed
    exit 1
fi

## check that the chromosomes are correctly formatted (chrN, not N), and fix if not
NUM_CHR_MATCHES=`awk -F$'\t' '$1 ~ /chr/' ${INFILE}.parsed | wc -l`
if [ "${NUM_CHR_MATCHES}" -ne "${NUM_TOT_LINES}" ]; then
    ## first check that the chromosomes are all numeric or are called 'chr'
    NUM_MATCH_CHRS=`awk -F$'\t' '$1 ~ /chr/ || $1 ~ /^[0-9]+$/' ${INFILE}.parsed | wc -l`
    if [ "${NUM_MATCH_CHRS}" -ne "${NUM_TOT_LINES}" ]; then
	echo "Not all chromosomes are either numeric or formatted as chrN!"
	rm ${INFILE}.parsed
	exit 1
    fi
    ## now go through and add chr to any chromosomes that are numeric
    awk -F$'\t' 'BEGIN{OFS=FS} {if ($1 ~ /^[0-9]+$/) {$1="chr"$1} {print $0}}' ${INFILE}.parsed > \
	${INFILE}.chr_parsed
    mv ${INFILE}.chr_parsed ${INFILE}.parsed
fi
    
## if we make it to this point, move the parsed file to the input and run it
mv ${INFILE}.parsed ${INFILE}
exit 0
