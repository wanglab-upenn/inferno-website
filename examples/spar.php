<head>
<base target="_blank">

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
<link href="smdb-tab2.css" rel="stylesheet" type="text/css">
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
$(function() {
    $( "#uploadLog" ).hide();
    var jobTitleHeader = $( "#jobTitle" ).text().replace("Running job","Analysis results for job");
    $( "#jobTitle" ).text(jobTitleHeader);
    
    $( "#tabs" ).tabs();
    $( "#tabsUL" ).show();
   
   
});

$(function() {
    var icons = {
      header: "ui-icon-circle-arrow-e",
      activeHeader: "ui-icon-circle-arrow-s"
    };

$("#accordion").accordion({
    header: "h3",
    collapsible: true,
    autoHeight: false,
    navigation: true,
    heightStyle: "content",
    icons: icons
}); 

    $( "#toggle" ).button().click(function() {
      if ( $( "#accordion" ).accordion( "option", "icons" ) ) {
        $( "#accordion" ).accordion( "option", "icons", null );
      } else {
        $( "#accordion" ).accordion( "option", "icons", icons );
      }
    });
  });
</script>
</head>
<body>
<!--<H1>SPAR 1.0 Server</H1>-->
<H1><a href="http://tesla.pcbi.upenn.edu/~pkuksa/SPAR/spar.html">SPAR 1.0 Server</a></H1>
<?php
echo "<div id=\"uploadLog\">Loading...";
#$uploadProgress=$_SESSION["upload_progress_123"];
#echo "progress=$uploadProgress";
print_r($_SESSION);
echo "</div>";



$doCleanup=1;
require_once('html2pdf-4.5.1/html2pdf.class.php');
function printT($msg)
{
  $currtime = date(DATE_RFC2822);
  echo "<p>$currtime ..... $msg";
  ob_flush();
  flush();
}

function get_remote_file_size($url)
{
     $ch = curl_init($url);
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
     curl_setopt($ch, CURLOPT_HEADER, TRUE);
     curl_setopt($ch, CURLOPT_NOBODY, TRUE);
     curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // follow redirects
     $data = curl_exec($ch);
     $size = -1;
     if (!curl_errno($ch))
     {
       $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
     }
     curl_close($ch);
     return $size;
}


function progressCallback( $download_size, $downloaded_size, $upload_size, $uploaded_size )
{
    static $previousProgress = 0;
    if ($previousProgress==100) $previousProgress=0;    

    if ( $download_size == 0 )
        $progress = 0;
    else
        $progress = round( $downloaded_size * 100 / $download_size );
    
    if ( $progress > $previousProgress)
    {
        $previousProgress = $progress;
        #$fp = fopen( "$tmpDir/progress.txt", 'a' );
        #fputs( $fp, "$progress\n" );
        #fclose( $fp );
        if (($progress % 10)==0)
        {
          echo ".";
          $downloadedMB= round($downloaded_size / 1000000);
          #echo "${downloadedMB}MB";
        }
          #echo "...$progress%";
        ob_flush();
        flush();
        #sleep(1);
    }
}



function downloadFile($fileURL, $outFile)
{
  $targetFile = fopen( $outFile, 'w' );
  $ch = curl_init( $fileURL );
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // follow redirects
  curl_setopt( $ch, CURLOPT_NOPROGRESS, false );
  curl_setopt( $ch, CURLOPT_PROGRESSFUNCTION, 'progressCallback' );
  curl_setopt( $ch, CURLOPT_FILE, $targetFile );
  curl_exec( $ch );
  curl_close( $ch );
  fclose( $targetFile );
  $fsize=filesize($outFile);
  #return $fsize;
  if ($fsize)
  {
    echo "DONE. ";
    $dispFileSize=$fsize/1000000;
    echo "($dispFileSize MB)";
  }
  else
  {
    exit("Could not download $fileURL");
  }

}

function printFileSize($filename)
{
  #$fsize=number_format(filesize($filename) / 1000000, 4, '.', '');
  
  #echo " [$fsize MB]";
  $fsize=filesize($filename);
  $sizepower = max(floor((strlen($fsize)-1)/3),0);
  $fsizestr = sprintf("%.2f", $fsize / pow(1024,$sizepower));
  $sizes=array("B","KB","MB","GB","TB","PB");
  echo " [ $fsizestr $sizes[$sizepower] ]";
}

function printAsFileSize($size)
{
  #$fsize=number_format(filesize($filename) / 1000000, 4, '.', '');
  
  #echo " [$fsize MB]";
  $sizepower = floor((strlen($size)-1)/3);
  $fsizestr = sprintf("%.2f", $size / pow(1024,$sizepower));
  $sizes=array("B","KB","MB","GB","TB","PB");
  echo " [ $fsizestr $sizes[$sizepower] ]";
}

# handle UPLOAD
#echo "<p>PHP info<br>";
#phpinfo();
#echo "<p>_POST<br>";
#print_r($_POST);
#echo "<p>_FILES<br>";
#print_r($_FILES);
#exit("here");

if (empty($_POST) && $_SERVER['REQUEST_METHOD'] == 'POST')
{
  $postMax = ini_get('post_max_size'); //grab the size limits...
  $contentLength=$_SERVER['CONTENT_LENGTH'];
  echo "<p>ERROR: \$_POST is empty!";
  echo "<p>CONTENT_LENGTH=$contentLength";
  if ($postMax < $contentLength/pow(2,20))
     echo "<p>Submission size have exceeded the post max $postMax</p>";
  exit("");
}

if (isset($_POST['submit_uploadYourBAM']))
{
echo "<div id=\"uploadLog\">";

printT("Uploading selected file");


session_cache_limiter('nochache');
session_start();
$uploaddir = 'uploads/';
echo '<pre>';
$uploadField="userBAMupload";
#print_r($_FILES);
try {

if (
        !isset($_FILES[$uploadField]['error']) ||
        is_array($_FILES[$uploadField]['error'])
    ) {
        throw new RuntimeException('ERROR: invalid upload.');
    }


$filesizelimit=10000000000;
if ($_FILES[$uploadField]['size'] > $filesizelimit) {
        #echo "ERROR: Exceeded filesize limit.\n";
        throw new RuntimeException("Exceeded filesize limit ($filesizelimit bytes)");
}


switch ($_FILES[$uploadField]['error']) {
        case UPLOAD_ERR_OK:
            #echo "No errors\n";
            break;
        case UPLOAD_ERR_NO_FILE:
            throw new RuntimeException('No file sent.');
            echo "ERROR: no file\n";
        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_FORM_SIZE:
            echo "ERROR: file size\n";
            throw new RuntimeException('Exceeded filesize limit.');
        default:
            echo "ERROR: unknown error\n";
            throw new RuntimeException('Unknown errors.');
}

$userfilename = $_FILES[$uploadField]['name'];
#echo "userfilename=$userfilename\n";

if (!preg_match("/\.(bam)$/i",$userfilename))
{
  echo "Not uploaded.\nOnly BAM files are accepted!\n";
}
else
{

$uploadfile = $uploaddir . basename($_FILES[$uploadField]['name']);
$tmpfile=$_FILES[$uploadField]['tmp_name'];
#echo "TMP_NAME=$tmpfile";
if (move_uploaded_file($_FILES[$uploadField]['tmp_name'], $uploadfile)) {
    #echo "File is valid, and was successfully uploaded.\n";
    $shortname=substr($userfilename,0,15);
    if (strlen($userfilename)>15)
      $shortname="$shortname...";
    #echo "\nFile $shortname was successfully uploaded.\n";
    printT("File $shortname was successfully uploaded");

    $_SESSION['userBAMupload']=$uploadfile;
    $_SESSION['BAM_file_is_uploaded']="yes";
} else {
    echo "Possible file upload attack!\n";
}

}
} catch (RuntimeException $e) {
  echo $e->getMessage();
}

#echo 'Here is some more debugging info:';
#print_r($_FILES);
#print "TMPFILE=$tmpfile\n";
#echo getcwd() . "\n";
#print_r($_POST);
print "</pre>";
#$_FILES=array();
#session_write_close();
#exit("");

    printT("Submitting uploaded file $shortname for analysis...");
    echo "</div>";
}



# END handle UPLOAD

session_cache_limiter('nochache');
session_start();

$genomeType="hg19";

if (!empty($_POST['genomeBuild']))
   $genomeType=htmlspecialchars($_POST['genomeBuild']);


$baseURL="http://faraday.pcbi.upenn.edu/~pkuksa/SPAR";
$baseURL="https://www.niagads.org/~pkuksa/SPAR";
$baseURL="http://tesla.pcbi.upenn.edu/~pkuksa/SPAR";
$baseJBrowseURL="$baseURL/jbrowse/JBrowse-1.12.0";
$defaultGenCoord="chr1:1102484-1102578";
$defaultGenCoord="chr9:96938223-96938334";
$defaultGenCoordJB="chr1%3A1102484..1102578";


$defaultGenCoordJB="chr9%3A96938223..96938334";

switch ($genomeType) {
  case "hg19":
    $defaultGenCoordJB="chr9:96938223..96938334";
    break;
  case "hg38":
    $defaultGenCoordJB="chr9:94175957..94176036";
    break;
  case "mm10":
    $defaultGenCoordJB="chr9:108568324..108568347";
    break;
}


echo "<script type=\"text/javascript\">";
echo "function bgshowhide(id) { var e = document.getElementById(id); e.style.display = (e.style.display == 'block') ? 'none' : 'block'; }";
echo "</script>";


// make sure output buffering is off before we start it
// this will ensure same effect whether or not ob is enabled already
while (ob_get_level()) {
    ob_end_flush();
}
// start output buffering
if (ob_get_length() === false) {
    ob_start();
}
   # check is custom annotation is requested
   $customAnnot="";
   $doCustomAnnot=0;
   #echo "_FILES=\n";
   #print_r($_FILES['userfile']);
   if (!empty($_SESSION['custom_annot']))
   {
     $customAnnot=$_SESSION['custom_annot'];
     if (!empty($customAnnot) && filesize($customAnnot))
     {
       //echo "CUSTOM_ANNOT=$customAnnot\n";
       $doCustomAnnot=1;
     }
   }
   if (!empty($_SESSION['userBAMupload']))
   {
     $userBAMupload=$_SESSION['userBAMupload'];
   }
   unset($_SESSION['custom_annot']);
   $_FILES = array();    
   $_SESSION = array();
   session_destroy();
   session_unset();

   $randomStr = substr( md5(rand()), 0, 7);
   $tmpDir = "SPAR_out/$randomStr";
   #echo "<base href=\"$tmpDir\">";
   echo "<h2 id=\"jobTitle\">Running job $randomStr</h2>";

   #echo $tmpDir;
   $output=shell_exec("mkdir -p $tmpDir");
   $output=shell_exec("chmod a+wrx $tmpDir");

   #echo "<p>Dataset: <a href=\"$dataset\">$dataset</a>\n";
   $dataset="";
   $strand="pos";
   
   
   if (isset($_POST['submit_DASHR']) && !empty($_POST['datasetDASHR'])) 

   #if (!empty($_POST['datasetDASHR'])) 
   {
     # DASHR
     $dataset = htmlspecialchars($_POST['datasetDASHR']);
     $positionPos=strpos($dataset,".pos.");
     if ($positionPos != FALSE)
     {
       $datasetPOS=$dataset;
       $datasetNEG=preg_replace('/.pos.bigWig/', ".neg.bigWig", $dataset);
        # print WHAT is being analyzed
        echo "<p>Input files:<br>$datasetPOS<br>$datasetNEG</p>";
     }
   }
   else if (isset($_POST['submit_ENCODE']) && !empty($_POST['datasetENCODE']))
   #else if (!empty($_POST['datasetENCODE']))
   {
      # ENCODE   
      $dataset=htmlspecialchars($_POST['datasetENCODE']);
      $positionPos=strpos($dataset,"Plus");
      if ($positionPos != FALSE)
      {
        $datasetPOS=$dataset;
        $datasetNEG=preg_replace('/Plus/',"Minus",$dataset);  
        # print WHAT is being analyzed
        echo "<p>Input files:<br>$datasetPOS<br>$datasetNEG</p>";
      }
      if ($position != FALSE)
      {
        $strand="neg";
      }
   } # end if DASHR or ENCODE dataset selected
   else if (isset($_POST['submit_ENCODEsmall']) && !empty($_POST['datasetENCODEsmall'])) 
   {
       $urlPOSNEG=htmlspecialchars($_POST['datasetENCODEsmall']);
       $tokens=split(";",$urlPOSNEG);
       if ($tokens[1]=="pos")
       {
         $datasetPOS=$tokens[0];
         $datasetNEG=$tokens[2];
       }
       else
       {
         $datasetPOS=$tokens[2];
         $datasetNEG=$tokens[0];
       }

   }
   else if (!empty($_POST['userTrackURLpos'])
            && !empty($_POST['userTrackURLneg'])
            && isset($_POST['submit_BigWig'])
           )
   {
      $datasetPOS=htmlspecialchars($_POST['userTrackURLpos']);
      $datasetNEG=htmlspecialchars($_POST['userTrackURLneg']);
      # print WHAT is being analyzed
      echo "<p>Input files:<br>$datasetPOS<br>$datasetNEG</p>";
      $dataset=$datasetPOS;
      $headersPOS=get_headers($datasetPOS);
      $headersNEG=get_headers($datasetNEG);
      #$pos1=stripos($headersPOS[0],"200 OK");
      #$pos2=stripos($headersNEG[0],"200 OK");
      $pos1=intval(substr($headersPOS[0],9,3)) < 400;
      $pos2=intval(stripos($headersNEG[0],9,3)) < 400;
      if ($pos1 === false || $pos2 === false)
      {
       if ($pos1 === false) { echo "<p>Track $datasetPOS not found\n<br>\n"; print_r($headersPOS); }
       if ($pos2 === false) { echo "<p>Track $datasetNEG not found\n<br>\n"; print_r($headersNEG); }
       #print_r($headersNEG);
       #echo stripos($headersPOS[0],"200 OK");
        
       exit("<p>Please provide valid URLs for both positive and negative signal tracks\n");
       }
   }
   else if (!empty($_POST['userBAM'])
            && isset($_POST['submit_BAM'])
           )
   {
      
      $userBAM = htmlspecialchars($_POST['userBAM']);
      $datasetBAM = "$tmpDir/input.bam";
      # check if BAM exists
      $headersBAM=get_headers($userBAM);
      $statusBAM=intval(substr($headersBAM[0],9,3)) < 400;
      if ($statusBAM === false)
      {
         echo "<p>BAM $userBAM not found<br>\n";
         print_r($headersBAM);
         exit("<p>Please provide valid URL for BAM file</p>");
      }
      # print WHAT is being analyzed
      echo "<p>Input file: $userBAM</p>";
   }
   #else if (!empty($userBAMupload) && isset($_POST['submit_uploadBAM']))
   else if (!empty($userBAMupload) && isset($_POST['submit_uploadYourBAM']))
   {
      $userBAM = "$baseURL/$userBAMupload"; 
      $datasetBAM = "$tmpDir/input.bam";
      # print WHAT is being analyzed
      echo "<p>Input file: $userBAMupload</p>";
      #echo "userBAM=$userBAM";
      #exit;
   }
   else {
     exit("Please select dataset or provide accessible URLs for both positive and negative signal tracks");
   }

    # analysis params
   $maxReadLengthDefault=44;
   $minReadLengthDefault=14;
   $minCoverageDefault=10;
   $minFoldChangeDefault=2;
   $options=array('options'=>array('default'=>$maxReadLengthDefault,'min_range'=>14,'max_range'=>200));
   $maxReadLength=filter_input(INPUT_POST, 'maxReadLen', FILTER_VALIDATE_INT, $options);
   $options=array('options'=>array('default'=>$minReadLengthDefault,'min_range'=>12,'max_range'=>200));
   $minReadLength=filter_input(INPUT_POST, 'minReadLen', FILTER_VALIDATE_INT, $options);
   $options=array('options'=>array('default'=>$minCoverageDefault,'min_range'=>1,'max_range'=>2000000));
   $minCoverage=filter_input(INPUT_POST, 'minCov', FILTER_VALIDATE_INT, $options);
   $options=array('options'=>array('default'=>$minFoldChangeDefault,'min_range'=>2,'max_range'=>2000));
   $minFoldChange=filter_input(INPUT_POST, 'minFoldChange', FILTER_VALIDATE_INT, $options);
   #echo "$minReadLength, $maxReadLength, $minCoverage";

   #echo("<h3>Processing job $randomStr</h3>");

   
   if ($doCustomAnnot==1)
     include "spar_tabs_hdr_with_custom.html";
   else 
     include "spar_tabs_hdr.html";


   echo "<div id=\"runLog\">";
    
   echo("<p>Genome: $genomeType</p>");
   #echo "<p>default coord=$defaultGenCoordJB</p>";
   #printT("Loading data");

   ob_flush();
   flush();

   exec("mkdir -p $tmpDir/tracks");
   $BIGWIGPOS="$tmpDir/input.pos.bigWig";
   $BIGWIGNEG="$tmpDir/input.neg.bigWig";
   $BIGWIGPOS="$tmpDir/tracks/raw.pos.bigWig";
   $BIGWIGNEG="$tmpDir/tracks/raw.neg.bigWig";
 
   
   if (!empty($datasetBAM) && !empty($userBAM))
   {   
       #downloadFile($userBAM,$datasetBAM);
       $displayDataName=preg_replace('/^.+\//',"", $userBAM);
       #$BIGWIGPOS="$datasetBAM.pos.bigWig";
       #$BIGWIGNEG="$datasetBAM.neg.bigWig";
   }
   else
   { 
     if (empty($datasetPOS) || empty($datasetNEG))
       exit("Please select dataset or provide accessible URLs for both positive and negative strand data");

     # dowload PLUS file
     printT("Loading PLUS strand: <a href=\"$datasetPOS\" target=\"_blank\">$datasetPOS</a> ");
     downloadFile($datasetPOS,$BIGWIGPOS);
     ob_flush();
     flush();

     # dowload MINUS file
     printT("Loading MINUS strand: <a href=\"$datasetNEG\" target=\"_blank\">$datasetNEG</a>");
     downloadFile($datasetNEG,$BIGWIGNEG);
     #echo "<p>BIGWIGNEG=$BIGWIGNEG";
     ob_flush();
     flush();
     $displayDataName=preg_replace('/^.+\//',"", $datasetPOS);
     #$displayDataName=preg_replace('/\..+$/',"", $displayDataName);
   }

   // adjust if bigWig is RPM!!!
   #if (isset($_POST['submit_ENCODEsmall']) && !empty($_POST['datasetENCODEsmall'])) 
   #{
     #$libSizePOS=exec("bash SPAR-master/get_libsize_bigwig.sh $BIGWIGPOS");
     #$libSizeNEG=exec("bash SPAR-master/get_libsize_bigwig.sh $BIGWIGNEG");
     //echo $libSizePOS;
     //echo $libSizeNEG;
     #$libSize=(int)$libSizePOS+(int)$libSizeNEG;
     #printT("Library size: $libSize (POS=$libSizePOS; NEG=$libSizeNEG)");
     #$minCoverage=10.0/$libSize*1000000;
   #}

   printT("Starting SPAR run");
   printT("Analysis parameters: min. fold change=$minFoldChange; min. coverage=$minCoverage; min. size=$minReadLength; max. size=$maxReadLength</p>");  

   $configFileSPAR="SPAR-master/config.$genomeType.sh";
   if (!copy($configFileSPAR,"$tmpDir/config.$genomeType.sh"))
   {
      exit("ERROR: failed to copy $configFileSPAR");
   }
   $configFileSPAR="$tmpDir/config.$genomeType.sh";
   file_put_contents($configFileSPAR,"export minReadLength=$minReadLength\n", FILE_APPEND | LOCK_EX);
   file_put_contents($configFileSPAR,"export maxReadLength=$maxReadLength\n", FILE_APPEND | LOCK_EX);
   file_put_contents($configFileSPAR,"export minCoverage=$minCoverage\n", FILE_APPEND | LOCK_EX);
   file_put_contents($configFileSPAR,"export minFoldChange=$minFoldChange\n", FILE_APPEND | LOCK_EX);

   $logFile="$tmpDir/input.$strand.bigWig.SPAR.log";
   if (!empty($datasetBAM))
      $logFile="$datasetBAM.SPAR.log";
   $logFile="$tmpDir/logs/SPAR.log";
   $RlogFile="$tmpDir/logs/R.log"; 
   exec("mkdir -p $tmpDir/logs");
   $p = NULL;
   if (!empty($userBAM) && !empty($datasetBAM))
   {
     #$p = popen("bash run_SPAR.sh $datasetBAM $tmpDir $strand 2>&1",'r');
     $userBAMsize=get_remote_file_size($userBAM);
     #if ($userBAMsize>0)
     #  echo "BAM size=$userBAMsize";
     #else
     #  echo "BAM size is unknown"; 
     #$p = popen("bash run_SPAR.sh $userBAM $tmpDir $strand $configFileSPAR 2>&1",'r');
     #echo "$userBAM";
     #echo "bash run_SPAR.sh $userBAM $tmpDir $strand $configFileSPAR > $logFile 2>&1";
     $p = popen("bash run_SPAR.sh $userBAM $tmpDir $strand $configFileSPAR  2>&1 | tee -a $logFile",'r');
     
   }
   else
   {
     #echo "<p>dataset=$dataset";
     #$p = popen("bash run_SPAR.sh $dataset $tmpDir $strand 2>&1", 'r');
     echo "bash run_SPAR.sh $BIGWIGPOS $tmpDir $strand $configFileSPAR 2>&1";
     $p = popen("bash run_SPAR.sh $BIGWIGPOS $tmpDir $strand $configFileSPAR 2>&1 | tee -a $logFile", 'r');
   }
   $isDone=FALSE;
   #echo "<p>";
   printT("Processing job $randomStr");
   while(!feof($p)) {
    $s=fgets($p);
    if (!$isDone)
       if (preg_match("/Loading BAM/",$s))
       { echo "<p>$s";
         if ($userBAMsize>0)
           printAsFileSize($userBAMsize);
       }
       else if (preg_match("/Loaded BAM/",$s))
       {
        echo "<p>$s";
        if ($userBAMsize>0)
          printAsFileSize($userBAMsize);
        echo "<p>";
        #printT("Starting analysis");
       }
       else if (preg_match("/^Encountered/",$s))
       {
         printT("$s");
       }
       else echo ".";
       #echo "<p>$s\n";
       #echo ".";
    #  echo "<p>$s\n";
    if (strpos($s,"DONE")!=FALSE) 
    {
       echo "<p>$s\n";
       #echo "<p>Sending results to the web server\n";
       printT("Sending results to the web server");
       #$isDone=TRUE;
    }
    #if (strpos($s,"reads / hour")!=FALSE)
    if (strpos($s,"reads / second")!=FALSE)
    {
       echo "<p>$s\n";
       $isDone=TRUE;
    }
    ob_flush();
    flush();
   }
   pclose($p);   

   printT("Creating plots");

   #$mp_output=exec("bash $tmpDir/make_plots.sh",$mp_out,$mp_ret_var);


   $mp_output=exec("bash $tmpDir/make_plots_parallel.sh",$mp_out,$mp_ret_var);

   if ($mp_ret_var == 0)
   {
     printT("DONE creating plots<br>");
   }
   else
   {
     echo "<p>RET_VAR=$mp_ret_var";  
     var_dump($mp_out);
     exit("Run failed");
   }
   
      
   #$logFile="$tmpDir/input.$strand.bigWig.SPAR.log";
   #if (!empty($datasetBAM))
   #   $logFile="$datasetBAM.SPAR.log";


   #echo "<div id=\"runLog\">";
   echo "<p><a href=\"javascript:bgshowhide('logFile')\">SPAR run log</a>";
   echo "<div id=\"logFile\" style=\"display:none;width:100%\">";
   echo "<pre>";
   include "$logFile";
   echo "</pre>";
   echo "</div>";

   echo "</div>"; # <div id=runLog>
 

   ob_flush();
   flush();

   #echo "<div style=\"overflow: scroll; height: 800px;\">";

   #if ($doCustomAnnot==1)
   #  include "spar_tabs_hdr_with_custom.html";
   #else 
   #  include "spar_tabs_hdr.html";

   #echo "<div id=\"accordion\">";
   $runSummaryHtml="$tmpDir/run_summary.html";
#$baseOutName="$tmpDir/input.$strand.bigWig";
$baseOutName="$tmpDir/input";
if (!empty($datasetBAM))
  $baseOutName="$tmpDir/input.bam";

$bigBed="$baseOutName.$strand.bedgraph.segm.bigBed";
$bigBedPOS="$baseOutName.pos.bedgraph.segm.bigBed";
$bigBedNEG="$baseOutName.neg.bedgraph.segm.bigBed";
$annotFile="$baseOutName.annot.final.xls";
$unannotFile="$baseOutName.unannot.xls";
$matSeqAnnot="$baseOutName.annot.mature_seq.xls";
$matSeqUnannot="$baseOutName.unannot.mature_seq.xls";
$geneExprTable="$baseOutName.gene_expression.xls";
$peaksTable="$baseOutName.all_peaks.xls";
$peaksBED="$baseOutName.all_peaks.bed";
$htmlPeakTable="$baseOutName.peak_table.html";
$annotSummaryTableHtml="$baseOutName.mapped_reads_annotation_summary.txt.html";

$baseOutName="$tmpDir";
$bigBedPOS="$baseOutName/tracks/peaks.pos.bigBed";
$bigBedNEG="$baseOutName/tracks/peaks.neg.bigBed";
$annotFile="$baseOutName.annot.final.xls";
$unannotFile="$baseOutName.unannot.xls";
$matSeqAnnot="$baseOutName/results/mature_seqs_annot.xls";
$matSeqUnannot="$baseOutName/results/mature_seqs_unannot.xls";
$geneExprTable="$baseOutName.gene_expression.xls";
$peaksTable="$baseOutName.all_peaks.xls";
$peaksBED="$baseOutName.all_peaks.bed";
$htmlPeakTable="$baseOutName/results/peak_browser.html";

$annotSummaryTableHtml="$baseOutName/results/annotation_summary.txt.html";


   #echo "<h3>Summary</h3>";
   echo "<div id=\"summary\">";
   #echo "<p>Dataset: $displayDataName";
   echo "<p>Link to results: <a href=\"$baseURL/$tmpDir\" target=\"_blank\">$baseURL/$tmpDir</a> (will be kept for 2 weeks)";
   echo "<p>Link to results: <a href=\"zipdownload.php?folder=$tmpDir\">Download results (ZIP)</a> (will be kept for 2 weeks)";

   #echo "<h3>QC report</h3>";
   echo "<h3>Data summary</h3>";
   include "$runSummaryHtml";

   echo "<h3>Summary of called peaks</h3>";
   include "$annotSummaryTableHtml";

   echo "</div>";

$ucscTracksFile="$tmpDir/ucsc_tracks.txt";
$ucscTracksFileSegm="$tmpDir/ucsc_tracks.segm.txt";
$ucscTracksFileRaw="$tmpDir/ucsc_tracks.raw.txt";
$ucscTracksFile="$tmpDir/tracks/ucsc_tracks.txt";
$ucscTracksFileSegm="$tmpDir/tracks/ucsc_tracks.segm.txt";
$ucscTracksFileRaw="$tmpDir/tracks/ucsc_tracks.raw.txt";

 
  echo "<div id=\"tableBrowser\">";
  #echo "<p>$displayDataName</p>";
  #$cmd_add_ucsc_url="bash SPAR-master/scripts/add_ucsc_tracks_url.sh $htmlPeakTable  \"$baseURL/$tmpDir/ucsc_tracks.raw.txt\" > $htmlPeakTable.with_tracks && mv $htmlPeakTable.with_tracks $htmlPeakTable";
  $cmd_add_ucsc_url="bash SPAR-master/scripts/add_ucsc_tracks_url.sh $htmlPeakTable  \"$baseURL/$ucscTracksFileRaw\" > $htmlPeakTable.with_tracks && mv $htmlPeakTable.with_tracks $htmlPeakTable";
  #echo "$cmd_add_ucsc_url";
  shell_exec($cmd_add_ucsc_url);
  #echo "<p><a href=\"$htmlPeakTable\">Browse peaks (expressed small RNAs)</a></p>";
  echo "<p><a href=\"$htmlPeakTable\" target=\"iframe_table\">Browse peaks (expressed small RNAs)</a></p>";
  #echo "<iframe name=\"iframe_table\" src=\"\" width=\"100%\" height=\"100%\" frameBorder=\"0\"></iframe>";
  echo "<iframe name=\"iframe_table\" src=\"\" height=\"50%\" width=\"100%\" frameBorder=\"0\"></iframe>";
  echo "</div>";


#$logFile="$tmpDir/input.$strand.bigWig.SPAR.log";
#if (!empty($datasetBAM))
#  $logFile="$datasetBAM.SPAR.log";
   #echo "<h3>Run Log</h3>";

/*
   echo "<div id=\"runLog\">";
   echo "<p><a href=\"javascript:bgshowhide('logFile')\">SPAR run log</a>";
   echo "<div id=\"logFile\" style=\"display:none;width:100%\">";
   echo "<pre>";
   include "$logFile";
   echo "</pre>";
   echo "</div>";
   echo "</div>";
*/ 

$trackName=preg_replace('/^.+\//',"",$dataset);
#$trackURLucsc="https://genome.ucsc.edu/cgi-bin/hgTracks?db=hg19&position=chr1:1103230-1103350&hgct_customText=track%20type=bigBed%20name=calledPeaksTracks%20description=%22$trackName%22%20visibility=full%20bigDataUrl=http://faraday.pcbi.upenn.edu/~pkuksa/SPAR/$tmpDir/input.bigWig.$strand.bedgraph.segm.bigBed";
$trackURLucsc="https://genome.ucsc.edu/cgi-bin/hgTracks?db=hg19&position=chr1:1103230-1103350&hgct_customText=track%20type=bigBed%20name=SPARcalledPeaks%20description=%22$trackName%22%20visibility=full%20bigDataUrl=$baseURL/$bigBed";

#$ucscTracksUrl="http://genome.ucsc.edu/cgi-bin/hgTracks?org=human&position=$genPosition&hgt.customText=$baseURL/$ucscTracksFile";
#$ucscTracksUrl="http://genome.ucsc.edu/cgi-bin/hgTracks?org=human&position=chr1:1102484-1102578&hgt.customText=$baseURL/$ucscTracksFile";
$ucscTracksUrl="http://genome.ucsc.edu/cgi-bin/hgTracks?db=$genomeType&position=chr1:1102484-1102578&hgt.customText=$baseURL/$ucscTracksFile";

#browser position chr1:1102484-1102578
#track type=bigWig name="Adipose1 [+]" description="Adipose1 [+] read coverage" bigDataUrl=https://niagads.org/~pkuksa/smRNA/tracks/hg19/adipose1.multi.pos.bigWig db=hg19
#track type=bigWig name="Adipose1 [-]" description="Adipose1 [-] read coverage" bigDataUrl=https://niagads.org/~pkuksa/smRNA/tracks/hg19/adipose1.multi.neg.bigWig db=hg19


$trackNamePOS="SPAR Called Peaks $displayDataName (PLUS strand)";
$trackNameNEG="SPAR Called Peaks $displayDataName (MINUS strand)";
$trackNameRawPOS="SPAR Raw Signal $displayDataName (PLUS strand)";
$trackNameRawNEG="SPAR Raw Signal $displayDataName (MINUS strand)";
$browserLine="browser position chr1:1102484-1102578";
$trackLinePOS="track type=bigBed name=\"$displayDataName +\" description=\"$trackNamePOS\" bigDataUrl=$baseURL/$bigBedPOS db=$genomeType";
$trackLineNEG="track type=bigBed name=\"$displayDataName -\" description=\"$trackNameNEG\" bigDataUrl=$baseURL/$bigBedNEG db=$genomeType";
#$trackLineRawPOS="track type=bigWig name=\"$displayDataName +\" description=\"$trackNameRawPOS\" visibility=2 color=100,0,0 bigDataUrl=$baseURL/$BIGWIGPOS db=$genomeType";
#$trackLineRawNEG="track type=bigWig name=\"$displayDataName -\" description=\"$trackNameRawNEG\" visibility=2 color=0,0,255 bigDataUrl=$baseURL/$BIGWIGNEG db=$genomeType";
$trackLineRawPOS="track type=bigWig name=\"$displayDataName + RAW\" description=\"$trackNameRawPOS\" visibility=2 color=100,0,0 bigDataUrl=$baseURL/$BIGWIGPOS db=$genomeType";
$trackLineRawNEG="track type=bigWig name=\"$displayDataName - RAW\" description=\"$trackNameRawNEG\" visibility=2 color=0,0,255 bigDataUrl=$baseURL/$BIGWIGNEG db=$genomeType";

   #file_put_contents( $ucscTracksFile, "$browserLine\n$trackLinePOS\n$trackLineNEG\n");
   file_put_contents( $ucscTracksFile, "$browserLine\n$trackLinePOS\n$trackLineNEG\n$trackLineRawPOS\n$trackLineRawNEG");
   file_put_contents( $ucscTracksFileSegm, "$trackLinePOS\n$trackLineNEG\n");
   file_put_contents( $ucscTracksFileRaw, "$trackLineRawPOS\n$trackLineRawNEG\n");


# add JBrowse


#addTracks=%5B%7B"label"%3A"mytrack"%2C"type"%3A"JBrowse%2FView%2FTrack%2FWiggle%2FXYPlot"%2C"key"%3A"coveragePlot"%2C"category"%3A"quant"%2C"urlTemplate"%3A"..%2Fadipose1.multi.pos.bigWig"%2C"storeClass"%3A"JBrowse%2FStore%2FSeqFeature%2FBigWig"%2C"max_score"%3A"1000"%7D%5D


#https://www.niagads.org/~pkuksa/SPAR/jbrowse/JBrowse-1.12.0/?loc=chr1%3A28833836..28834124&tracks=DNA%2CDASHRannotation%2Cmytrack&addTracks=%5B%7B%22label%22%3A%22mytrack%22%2C%22type%22%3A%22JBrowse%2FView%2FTrack%2FWiggle%2FXYPlot%22%2C%22key%22%3A%22coveragePlot%22%2C%22category%22%3A%22quant%22%2C%22urlTemplate%22%3A%22..%2Fadipose1.multi.pos.bigWig%22%2C%22storeClass%22%3A%22JBrowse%2FStore%2FSeqFeature%2FBigWig%22%2C%22autoscale%22%3A%22local%22%7D%5D&highlight=
#addTracks=[{"label":"mytrack","type":"JBrowse/View/Track/Wiggle/XYPlot","key":"coveragePlot","category":"quant","urlTemplate":"../adipose1.multi.pos.bigWig","storeClass":"JBrowse/Store/SeqFeature/BigWig","autoscale":"local"}]

#https://www.niagads.org/~pkuksa/SPAR/jbrowse/JBrowse-1.12.0/?loc=chr1%3A28833836..28834124&tracks=DNA%2CDASHRannotation%2Cmytrack&addTracks=%5B%7B%22label%22%3A%22mytrack%22%2C%22type%22%3A%22JBrowse%2FView%2FTrack%2FWiggle%2FXYPlot%22%2C%22key%22%3A%22coveragePlot%22%2C%22category%22%3A%22quant%22%2C%22urlTemplate%22%3A%22..%2F..%2F..%2FSPAR_out%2F75460a9%2Finput.pos.bigWig%22%2C%22storeClass%22%3A%22JBrowse%2FStore%2FSeqFeature%2FBigWig%22%2C%22autoscale%22%3A%22local%22%7D%5D
$jbrowseURLtemplate="$baseJBrowseURL/?loc=$defaultGenCoord&tracks=DNA%2CDASHRannotation&addTracks=%5B%7B%22label%22%3A%22TRACKLABEL%22%2C%22type%22%3A%22JBrowse%2FView%2FTrack%2FWiggle%2FXYPlot%22%2C%22key%22%3A%22TRACKNAME%22%2C%22category%22%3A%22SPARPeaks%22%2C%22urlTemplate%22%3A%22BWDATAURL%22%2C%22storeClass%22%3A%22JBrowse%2FStore%2FSeqFeature%2FBigWig%22%2C%22autoscale%22%3A%22local%22%7D%5D&tracklist=0";
$jbrowseURLtemplate="$baseJBrowseURL/?loc=chr1:1102284..1102683&tracks=DNA%2CDASHRannotation,TRACKLABEL&addTracks=%5B%7B%22label%22%3A%22TRACKLABEL%22%2C%22type%22%3A%22JBrowse%2FView%2FTrack%2FWiggle%2FXYPlot%22%2C%22key%22%3A%22TRACKNAME%22%2C%22category%22%3A%22SPARPeaks%22%2C%22urlTemplate%22%3A%22BWDATAURL%22%2C%22storeClass%22%3A%22JBrowse%2FStore%2FSeqFeature%2FBigWig%22%2C%22autoscale%22%3A%22local%22%7D%5D&tracklist=0";
$jbrowseURLtemplate="$baseJBrowseURL/?loc=chr9:96938223..96938334&tracks=DNA%2CDASHRannotation,TRACKLABELPOS,TRACKLABELNEG&addTracks=%5B%7B%22label%22%3A%22TRACKLABELPOS%22%2C%22type%22%3A%22JBrowse%2FView%2FTrack%2FWiggle%2FXYPlot%22%2C%22key%22%3A%22TRACKNAMEPOS%22%2C%22category%22%3A%22SPARPeaks%22%2C%22urlTemplate%22%3A%22BWDATAURLPOS%22%2C%22storeClass%22%3A%22JBrowse%2FStore%2FSeqFeature%2FBigWig%22%2C%22autoscale%22%3A%22local%22%7D%2C%7B%22label%22%3A%22TRACKLABELNEG%22%2C%22type%22%3A%22JBrowse%2FView%2FTrack%2FWiggle%2FXYPlot%22%2C%22key%22%3A%22TRACKNAMENEG%22%2C%22category%22%3A%22SPARPeaks%22%2C%22urlTemplate%22%3A%22BWDATAURLNEG%22%2C%22storeClass%22%3A%22JBrowse%2FStore%2FSeqFeature%2FBigWig%22%2C%22autoscale%22%3A%22local%22%7D%5D&tracklist=0";
$jbrowseURLtemplate="$baseJBrowseURL/?loc=chr9:96938223..96938334&tracks=DNA%2CDASHRannotation,TRACKLABELPOS,TRACKLABELNEG&addTracks=%5B%7B%22label%22%3A%22TRACKLABELPOS%22%2C%22type%22%3A%22JBrowse%2FView%2FTrack%2FWiggle%2FXYPlot%22%2C%22key%22%3A%22TRACKNAMEPOS%22%2C%22category%22%3A%22SPARPeaks%22%2C%22urlTemplate%22%3A%22BWDATAURLPOS%22%2C%22storeClass%22%3A%22JBrowse%2FStore%2FSeqFeature%2FBigWig%22%2C%22autoscale%22%3A%22local%22%7D%2C%7B%22label%22%3A%22TRACKLABELNEG%22%2C%22type%22%3A%22JBrowse%2FView%2FTrack%2FWiggle%2FXYPlot%22%2C%22key%22%3A%22TRACKNAMENEG%22%2C%22category%22%3A%22SPARPeaks%22%2C%22urlTemplate%22%3A%22BWDATAURLNEG%22%2C%22storeClass%22%3A%22JBrowse%2FStore%2FSeqFeature%2FBigWig%22%2C%22autoscale%22%3A%22local%22%7D%2C%7B%22label%22%3A%22RAWLABELNEG%22%2C%22type%22%3A%22JBrowse%2FView%2FTrack%2FWiggle%2FXYPlot%22%2C%22key%22%3A%22RAWNAMENEG%22%2C%22category%22%3A%22SPARraw%22%2C%22urlTemplate%22%3A%22BWRAWDATAURLNEG%22%2C%22storeClass%22%3A%22JBrowse%2FStore%2FSeqFeature%2FBigWig%22%2C%22autoscale%22%3A%22local%22%7D%5D&tracklist=0";
$jbrowseURLtemplate="$baseJBrowseURL/?loc=chr9:96938223..96938334&tracks=DNA%2CDASHRannotation,TRACKLABELPOS,TRACKLABELNEG&addTracks=%5B%7B%22label%22%3A%22TRACKLABELPOS%22%2C%22type%22%3A%22JBrowse%2FView%2FTrack%2FWiggle%2FXYPlot%22%2C%22key%22%3A%22TRACKNAMEPOS%22%2C%22category%22%3A%22SPARPeaks%22%2C%22urlTemplate%22%3A%22BWDATAURLPOS%22%2C%22storeClass%22%3A%22JBrowse%2FStore%2FSeqFeature%2FBigWig%22%2C%22autoscale%22%3A%22local%22%7D%2C%7B%22label%22%3A%22TRACKLABELNEG%22%2C%22type%22%3A%22JBrowse%2FView%2FTrack%2FWiggle%2FXYPlot%22%2C%22key%22%3A%22TRACKNAMENEG%22%2C%22category%22%3A%22SPARPeaks%22%2C%22urlTemplate%22%3A%22BWDATAURLNEG%22%2C%22storeClass%22%3A%22JBrowse%2FStore%2FSeqFeature%2FBigWig%22%2C%22autoscale%22%3A%22local%22%7D%2C%7B%22label%22%3A%22RAWLABELNEG%22%2C%22type%22%3A%22JBrowse%2FView%2FTrack%2FWiggle%2FXYPlot%22%2C%22key%22%3A%22RAWNAMENEG%22%2C%22category%22%3A%22SPARraw%22%2C%22urlTemplate%22%3A%22BWRAWDATAURLNEG%22%2C%22storeClass%22%3A%22JBrowse%2FStore%2FSeqFeature%2FBigWig%22%2C%22autoscale%22%3A%22local%22%7D%2C%7B%22label%22%3A%22RAWLABELPOS%22%2C%22type%22%3A%22JBrowse%2FView%2FTrack%2FWiggle%2FXYPlot%22%2C%22key%22%3A%22RAWNAMEPOS%22%2C%22category%22%3A%22SPARraw%22%2C%22urlTemplate%22%3A%22BWRAWDATAURLPOS%22%2C%22storeClass%22%3A%22JBrowse%2FStore%2FSeqFeature%2FBigWig%22%2C%22autoscale%22%3A%22local%22%7D%5D&tracklist=0";
$jbrowseURLtemplate="$baseJBrowseURL/?data=data/$genomeType&loc=chr9:96938223..96938334&tracks=DASHR$genomeType,TRACKLABELPOS,TRACKLABELNEG,RAWLABELPOS,RAWLABELNEG&addTracks=%5B%7B%22label%22%3A%22TRACKLABELPOS%22%2C%22type%22%3A%22JBrowse%2FView%2FTrack%2FWiggle%2FXYPlot%22%2C%22key%22%3A%22TRACKNAMEPOS%22%2C%22category%22%3A%22SPARPeaks%22%2C%22urlTemplate%22%3A%22BWDATAURLPOS%22%2C%22storeClass%22%3A%22JBrowse%2FStore%2FSeqFeature%2FBigWig%22%2C%22autoscale%22%3A%22local%22%7D%2C%7B%22label%22%3A%22TRACKLABELNEG%22%2C%22type%22%3A%22JBrowse%2FView%2FTrack%2FWiggle%2FXYPlot%22%2C%22key%22%3A%22TRACKNAMENEG%22%2C%22category%22%3A%22SPARPeaks%22%2C%22urlTemplate%22%3A%22BWDATAURLNEG%22%2C%22storeClass%22%3A%22JBrowse%2FStore%2FSeqFeature%2FBigWig%22%2C%22autoscale%22%3A%22local%22%7D%2C%7B%22label%22%3A%22RAWLABELNEG%22%2C%22type%22%3A%22JBrowse%2FView%2FTrack%2FWiggle%2FXYPlot%22%2C%22key%22%3A%22RAWNAMENEG%22%2C%22category%22%3A%22SPARraw%22%2C%22urlTemplate%22%3A%22BWRAWDATAURLNEG%22%2C%22storeClass%22%3A%22JBrowse%2FStore%2FSeqFeature%2FBigWig%22%2C%22autoscale%22%3A%22local%22%7D%2C%7B%22label%22%3A%22RAWLABELPOS%22%2C%22type%22%3A%22JBrowse%2FView%2FTrack%2FWiggle%2FXYPlot%22%2C%22key%22%3A%22RAWNAMEPOS%22%2C%22category%22%3A%22SPARraw%22%2C%22urlTemplate%22%3A%22BWRAWDATAURLPOS%22%2C%22storeClass%22%3A%22JBrowse%2FStore%2FSeqFeature%2FBigWig%22%2C%22autoscale%22%3A%22local%22%7D%5D&tracklist=0";
$jbrowseURLtemplate="$baseJBrowseURL/?data=data/$genomeType&loc=$defaultGenCoordJB&tracks=DASHR$genomeType,TRACKLABELPOS,TRACKLABELNEG,RAWLABELPOS,RAWLABELNEG&addTracks=%5B%7B%22label%22%3A%22TRACKLABELPOS%22%2C%22type%22%3A%22JBrowse%2FView%2FTrack%2FWiggle%2FXYPlot%22%2C%22key%22%3A%22TRACKNAMEPOS%22%2C%22category%22%3A%22SPARPeaks%22%2C%22urlTemplate%22%3A%22BWDATAURLPOS%22%2C%22storeClass%22%3A%22JBrowse%2FStore%2FSeqFeature%2FBigWig%22%2C%22autoscale%22%3A%22local%22%7D%2C%7B%22label%22%3A%22TRACKLABELNEG%22%2C%22type%22%3A%22JBrowse%2FView%2FTrack%2FWiggle%2FXYPlot%22%2C%22key%22%3A%22TRACKNAMENEG%22%2C%22style%22%3A%7B%22pos_color%22%3A%22red%22%7D%2C%22category%22%3A%22SPARPeaks%22%2C%22urlTemplate%22%3A%22BWDATAURLNEG%22%2C%22storeClass%22%3A%22JBrowse%2FStore%2FSeqFeature%2FBigWig%22%2C%22autoscale%22%3A%22local%22%7D%2C%7B%22label%22%3A%22RAWLABELNEG%22%2C%22type%22%3A%22JBrowse%2FView%2FTrack%2FWiggle%2FXYPlot%22%2C%22key%22%3A%22RAWNAMENEG%22%2C%22category%22%3A%22SPARraw%22%2C%22urlTemplate%22%3A%22BWRAWDATAURLNEG%22%2C%22style%22%3A%7B%22pos_color%22%3A%22red%22%7D%2C%22storeClass%22%3A%22JBrowse%2FStore%2FSeqFeature%2FBigWig%22%2C%22autoscale%22%3A%22local%22%7D%2C%7B%22label%22%3A%22RAWLABELPOS%22%2C%22type%22%3A%22JBrowse%2FView%2FTrack%2FWiggle%2FXYPlot%22%2C%22key%22%3A%22RAWNAMEPOS%22%2C%22category%22%3A%22SPARraw%22%2C%22urlTemplate%22%3A%22BWRAWDATAURLPOS%22%2C%22storeClass%22%3A%22JBrowse%2FStore%2FSeqFeature%2FBigWig%22%2C%22autoscale%22%3A%22local%22%7D%5D&tracklist=0";
#$jbrowseURLtemplate="$baseJBrowseURL/?loc=$defaultGenCoord&tracks=DNA";
#$jbrowseURLtemplate="$baseJBrowseURL";
$jbrowseURL=$jbrowseURLtemplate;
#$jbrowseURL=preg_replace('/TRACKLABEL/',"{$displayDataName}POS",$jbrowseURL);
#$jbrowseURL=preg_replace('/TRACKNAME/',"{$displayDataName} (+ strand)",$jbrowseURL);
$jbrowseURL=preg_replace('/TRACKLABELPOS/',"{$displayDataName}POS",$jbrowseURL);
$jbrowseURL=preg_replace('/TRACKNAMEPOS/',"{$displayDataName} PEAKS (+ strand)",$jbrowseURL);
#$jbrowseURL=preg_replace('/BWDATAURL/',"../../../$BIGWIGPOS",$jbrowseURL);
#$jbrowseURL=preg_replace('/BWDATAURLPOS/',"../../../$BIGWIGPOS",$jbrowseURL);
$SEGMBIGWIGPOS=preg_replace('/.bigWig/',".segm.bigWig",$BIGWIGPOS);
$SEGMBIGWIGPOS=preg_replace('/.bigBed/',".jbrowse.bigWig",$bigBedPOS);
#$SEGMBIGWIGPOS=preg_replace('/.bigWig/',".segm.bigWig",$BIGWIGPOS);
#$jbrowseURL=preg_replace('/BWDATAURL/',"../../../$SEGMBIGWIGPOS",$jbrowseURL);
$jbrowseURL=preg_replace('/BWDATAURLPOS/',"../../../$SEGMBIGWIGPOS",$jbrowseURL);
$jbrowseURL=preg_replace('/TRACKLABELNEG/',"{$displayDataName}NEG",$jbrowseURL);
$jbrowseURL=preg_replace('/TRACKNAMENEG/',"{$displayDataName} PEAKS (- strand)",$jbrowseURL);
$SEGMBIGWIGNEG=preg_replace('/.bigWig/',".segm.bigWig",$BIGWIGNEG);
$SEGMBIGWIGNEG=preg_replace('/.bigBed/',".jbrowse.bigWig",$bigBedNEG);
$jbrowseURL=preg_replace('/BWDATAURLNEG/',"../../../$SEGMBIGWIGNEG",$jbrowseURL);

# add RAW signal track (NEG)
$jbrowseURL=preg_replace('/RAWLABELNEG/',"{$displayDataName}NEGraw",$jbrowseURL);
$jbrowseURL=preg_replace('/RAWNAMENEG/',"{$displayDataName} RAW signal (- strand)",$jbrowseURL);
$jbrowseURL=preg_replace('/BWRAWDATAURLNEG/',"../../../$BIGWIGNEG",$jbrowseURL);

# add RAW signal track (POS)
$jbrowseURL=preg_replace('/RAWLABELPOS/',"{$displayDataName}POSraw",$jbrowseURL);
$jbrowseURL=preg_replace('/RAWNAMEPOS/',"{$displayDataName} RAW signal (+ strand)",$jbrowseURL);
$jbrowseURL=preg_replace('/BWRAWDATAURLPOS/',"../../../$BIGWIGPOS",$jbrowseURL);

#  $jbrowseURL="$baseJBrowseURL/?loc=$defaultGenCoord&tracks=DNA,DASHRannotation&addTracks=[{\"label\":\"{$displayDataName}POS\",\"type\":\"JBrowse/View/Track/Wiggle/XYPlot\",\"category\":\"SPAR Called Peaks\",\"key\":\"$displayDataName (+ strand)\",\"autoscale\":\"local\",\"storeClass\":\"JBrowse/Store/SeqFeature/BigWig\",\"urlTemplate\":\"../../../$BIGWIGPOS\"}]";
  #echo "$jbrowseURL";

# ADD JBrowse HERE
#  echo "<div style=\"margin: -1em 0 1em 0;\">";
#  echo "<p><iframe style=\"border: 1px solid #505050;\" src=\"$jbrowseURL\" height=\"600\" width=\"1000\"></iframe></p>";
#  echo "</div>";
 
#echo "<div style=\"margin: -1em 0 1em 0;\"><p><iframe style=\"border: 1px solid #505050;\" src=\"https://www.niagads.org/~pkuksa/SPAR/jbrowse/JBrowse-1.12.0/?loc=chr1%3A1102449..1102518&tracks=DNA,DASHRannotation&highlight=&tracklist=0\" height=\"450\" width=\"620\"></iframe></p></div>";
  
 


   #echo "<p><a href=\"$trackURLucsc\" target=\"_blank\">View SPAR peak track in UCSC Genome Browser</a>";
   #echo "<h3>View in Genome Browser</h3>";
   echo "<div id=\"viewInGenomeBrowser\">";
   echo "<p style=\"margin-bottom: 1cm;\"><a href=\"$ucscTracksUrl\" target=\"_blank\">View SPAR result tracks in UCSC Genome Browser</a> (opens in the new window)<br></p>";
   # ADD JBrowse HERE
   echo "<div style=\"margin: -1em 0 1em 0;\">";
   echo "<p><iframe style=\"border: 1px solid #505050;\" src=\"$jbrowseURL\" height=\"600\" width=\"1000\"></iframe></p>";
   echo "</div>";
 
   

   echo "</div>";
   #$tracklineUrl="http://genome.ucsc.edu/cgi-bin/hgTracks?org=human&position=$genPosition&hgt.customText=https://www.niagads.org/~pkuksa/SPAR/$tmpDir/tracklines.txt";
   #echo "<h3>Downloads</h3>";
   #echo "<div id=\"downloads\" style=\"overflow: scroll; height: 800px;\">";
   echo "<div id=\"downloads\">";


   include "$tmpDir/results.html";   


   if (!empty($datasetBAM))
   {
     echo "<h4>Mature sequences</h4>";
     echo "<p><a href=\"$baseURL/$matSeqAnnot\" download>Download annotated mature sequences [XLS]</a>";
     printFileSize($matSeqAnnot);
     echo "<p><a href=\"$baseURL/$matSeqUnannot\" download>Download un-annotated mature sequences [XLS]</a>";
     printFileSize($matSeqUnannot);
   }
   if ( 0 == 1 ) 
   {
   echo "<h4>Tracks genome-wide</h4>";
   echo "<p><a href=\"$baseURL/$bigBedPOS\" download>Download SPAR called peaks track (PLUS strand) [bigBED]</a>";
   printFileSize($bigBedPOS);
   echo "<p><a href=\"$baseURL/$bigBedNEG\" download>Download SPAR called peaks track (MINUS strand) [bigBED]</a>";
   printFileSize($bigBedNEG);
   echo "<p><a href=\"$baseURL/$BIGWIGPOS\" download>Download SPAR raw track (PLUS strand) [bigWig]</a>";
   printFileSize($BIGWIGPOS);
   echo "<p><a href=\"$baseURL/$BIGWIGNEG\" download>Download SPAR raw track (MINUS strand) [bigWig]</a>";
   printFileSize($BIGWIGNEG);
   }
   #echo "<p><a href=\"$baseURL/$bigBed\" download>Download SPAR peaks track</a>";
  
   if ( 0==1 )
   {
   echo "<h4>Tables genome-wide</h4>";
   echo "<p><a href=\"$baseURL/$geneExprTable\" download>Gene expression table (XLS)</a>";
   printFileSize($geneExprTable);
   echo "<p><a href=\"$baseURL/$peaksTable\" download>SPAR peaks (ALL) (XLS)</a>";
   printFileSize($peaksTable);
   echo "<p><a href=\"$baseURL/$peaksBED\" download>SPAR peaks (ALL) (BED)</a>";
   printFileSize($peaksBED);
   echo "<p><a href=\"$baseURL/$annotFile\" download>SPAR peaks (Annotated) (XLS)</a>";
   printFileSize($annotFile);
   echo "<p><a href=\"$baseURL/$unannotFile\" download>SPAR peaks (Un-annotated) (XLS)</a>";
   printFileSize($unannotFile);
   }
   # overlap with DASHR
   # currently, only hg19
   
   if ( 0 == 1 ) 
   {
   if (strcmp($genomeType,"hg19")==0)
   {
   echo "<h4>Comparison with DASHR</h4>";
   $annotDASHRexprFileOut="$annotFile.dashr.expr.intersect.xls";
   $DASHRtable="SPAR-master/annot/DASHR/dashr_annot_all_2.sorted.noheader.csv.with_anchors";
   $DASHRtable="SPAR-master/annot/DASHR/DASHR_tissue_annot_mergeall_PEAKS_pavel.unique.csv";
   $DASHRexpr="SPAR-master/annot/DASHR/dashr.annot.anchor.expression.table.max100";
   $DASHRexpr="SPAR-master/annot/DASHR/DASHR_tissue_annot_mergeall_PEAKS_pavel.unique.csv.expression_table";
   if ( 0 == 1 )
   {
   $d_output=exec("bash overlap_with_DASHR.sh $annotFile $DASHRtable $DASHRexpr",$d_out,$d_ret_val);
      if ($d_ret_val != 0)
        echo "DASHR annotation failed: $d_ret_val";
      else
      {
        echo "<p><a href=\"$baseURL/$annotDASHRexprFileOut\" download>SPAR peaks with DASHR expression profiles (annotated sncRNA loci) (XLS)</a>";
        printFileSize($annotDASHRexprFileOut);
      }

   # overlap with un-annotated loci found in DASHR 
   $unannotDASHRexprFileOut="$unannotFile.dashr.expr.intersect.xls";
   $DASHRtable="SPAR-master/annot/DASHR/DASHR_tissue_unannot_mergeall_PEAKS_pavel.unique.csv";
   $DASHRexpr="SPAR-master/annot/DASHR/DASHR_tissue_unannot_mergeall_PEAKS_pavel.unique.csv.expression_table";
   $d_output=exec("bash overlap_with_DASHR.sh $unannotFile $DASHRtable $DASHRexpr",$d_out,$d_ret_val);
      if ($d_ret_val != 0)
        echo "DASHR annotation failed: $d_ret_val";
      else
      {
        echo "<p><a href=\"$baseURL/$unannotDASHRexprFileOut\" download>SPAR peaks with DASHR expression profile (un-annotated sncRNA loci) (XLS)</a>";
        printFileSize($unannotDASHRexprFileOut);
      }
      $combinedDASHRexprFileOut="$baseOutName.combined.dashr.expr.intersect.xls";
      $d_output=exec("tail -n +2 $unannotDASHRexprFileOut | cat $annotDASHRexprFileOut - > $combinedDASHRexprFileOut", $d_out, $d_ret_val);
      if ($d_ret_val != 0)
        echo "DASHR annotation failed: $d_ret_val";
      else
      {
        echo "<p><a href=\"$baseURL/$combinedDASHRexprFileOut\" download>SPAR peaks with DASHR expression profile (annnotated and un-annotated sncRNA loci) (XLS)</a>";
        printFileSize($combinedDASHRexprFileOut);
      }
      $noDASHRoverlapFileOut="$baseOutName.peaks.noDASHRoverlap.xls";
      #echo("cat $baseOutName*.peaks_no_overlap | awk 'BEGIN{OFS=\"\\t\"}{print $1,$2,$3,$4,$19,$6} > $noDASHRoverlapFileOut");
      #$d_output=exec("cat $baseOutName*.peaks_no_overlap | awk 'BEGIN{OFS=\"\\t\"}{print $1,$2,$3,$4,$19,$6}' > $noDASHRoverlapFileOut", $d_out, $d_ret_val);
      $d_output=exec("cat $baseOutName*dashr.peaks_no_overlap > $noDASHRoverlapFileOut", $d_out, $d_ret_val);
    if ($d_ret_val != 0)
        echo "DASHR annotation failed: $d_ret_val";
      else
      {
        echo "<p><a href=\"$baseURL/$noDASHRoverlapFileOut\" download>SPAR peaks without overlap with DASHR (XLS)</a>";
        printFileSize($noDASHRoverlapFileOut);
      }
      }
      echo "<h4>Comparison with ENCODE</h4>";
      $annotENCODEexprFileOut="$annotFile.encode.expr.intersect.xls";
      $unannotENCODEexprFileOut="$unannotFile.encode.expr.intersect.xls";
      $combinedENCODEexprFileOut="$baseOutName.combined.encode.expr.intersect.xls";
      $noENCODEoverlapFileOut="$baseOutName.peaks.noENCODEoverlap.xls";
      echo "<p><a href=\"$baseURL/$annotENCODEexprFileOut\" download>SPAR peaks with ENCODE expression profiles (annotated sncRNA loci) (XLS)</a>";
      printFileSize($annotENCODEexprFileOut);
      echo "<p><a href=\"$baseURL/$unannotENCODEexprFileOut\" download>SPAR peaks with ENCODE expression profile (un-annotated sncRNA loci) (XLS)</a>";
      printFileSize($unannotENCODEexprFileOut);
      echo "<p><a href=\"$baseURL/$combinedENCODEexprFileOut\" download>SPAR peaks with ENCODE expression profile (annnotated and un-annotated sncRNA loci) (XLS)</a>";
      printFileSize($combinedENCODEexprFileOut);
      echo "<p><a href=\"$baseURL/$noENCODEoverlapFileOut\" download>SPAR peaks without overlap with ENCODE (XLS)</a>";
      printFileSize($noENCODEoverlapFileOut);
       
    }
   }
   if ($doCustomAnnot)
   {
      $annotFileCustOut="$annotFile.customAnnot.xls";
      $statCustFile="$annotFile.customAnnot.stats.html";
      #$ca_output=exec("bash annotate_custom.sh $annotFile $customAnnot > $annotFileCustOut",$ca_out,$ca_ret_val);
      $ca_output=exec("bash annotate_custom.sh $annotFile $customAnnot",$ca_out,$ca_ret_val);
      if ($ca_ret_val != 0)
        echo "Custom annotation failed: $ca_ret_val";
      else
        echo "<p><a href=\"$baseURL/$annotFileCustOut\" download>Annotated SPAR peaks (Custom annotation)</a>";
      
      $unannotFileCustOut="$unannotFile.customAnnot.xls";
      #$ca_output=exec("bash annotate_custom.sh $unannotFile $customAnnot > $unannotFileCustOut",$ca_out,$ca_ret_val);
      $ca_output=exec("bash annotate_custom.sh $unannotFile $customAnnot",$ca_out,$ca_ret_val);
      if ($ca_ret_val != 0)
        echo "Custom annotation failed: $ca_ret_val";
      else
        echo "<p><a href=\"$baseURL/$unannotFileCustOut\" download>Un-annotated SPAR peaks (Custom annotation)</a>";
   }

   echo "<h4>Download peak tables by RNA class</h4>";
   include "$annotSummaryTableHtml";   
   echo "</div>";
   if ($doCustomAnnot)
   {
     #echo "<h3>Custom annotation summary</h3>";
     echo "<div id=\"custAnnot\">";
     echo "<p>Custom annotation file used: $customAnnot";
     echo "<p><a href=\"$annotFile.customAnnot.overlapsOnly.xls\" download>Peaks within intervals</a>";
     echo "<br><a href=\"$annotFile.intersect.customAnnot.xls\" download>Intervals with peaks</a>";
     echo "<p>Annotated SPAR peaks";
     $statCustFile="$annotFile.customAnnot.stats.html";
     include "$statCustFile";
     echo "<p>Un-annotated SPAR peaks";
     echo "<p><a href=\"$unannotFile.customAnnot.overlapsOnly.xls\" download>Peaks within intervals</a>";
     echo "<br><a href=\"$unannotFile.intersect.customAnnot.xls\" download>Intervals with peaks</a>";
     $statCustFile="$unannotFile.customAnnot.stats.html";
     include "$statCustFile";
     echo "</div>";
   }



   #echo "<p>Dataset: $displayDataName";
  
   #echo "<h3>Plots</h3>";
   #echo "<div id=\"plots\">";
   #echo "<div id=\"plots\" style=\"overflow: scroll; height: 1200px;\">";
   #echo "<div id=\"plots\" style=\"overflow: scroll; height: 800px;\">";
   echo "<div id=\"plots\" style=\"height: 600px;\">";
   #echo "<div id=\"plots\">";
   $page = ob_get_contents();
   echo "<p>Hover to preview plots, click to see hi-res image in the new window</p>";
   #printT("Creating plots");

   #$mp_output=exec("bash $tmpDir/make_plots.sh",$mp_out,$mp_ret_var);

   /*
   $mp_output=exec("bash $tmpDir/make_plots_parallel.sh",$mp_out,$mp_ret_var);

   if ($mp_ret_var == 0)
   {
     printT("DONE creating plots<br>");
   }
   else
   {
     echo "<p>RET_VAR=$mp_ret_var";  
     var_dump($mp_out);
     exit("Run failed");
   }
   */
      
   include "$tmpDir/module3_annot.html";
   echo "</div>"; // plots

#echo "</div>"; // accordion

   
   $pageplots = ob_get_contents();
   $page = $pageplots;

   ob_flush();
   flush();


//$page = ob_get_contents();
ob_end_flush();
$tmpReplace=preg_replace("/\//","\/",$tmpDir);
$page=preg_replace("/\"$tmpReplace\//","\"$baseURL/$tmpDir/",$page);
$page=preg_replace("/<iframe.+iframe>/","",$page); #remove JBrowse from report
$page=preg_replace("/<(div id=\"[^>=\"]+)[^>]+overflow:[^>]+>/","<$1>",$page); # remove overflow divs
#   echo "<div id=\"plots\" style=\"overflow: scroll; height: 800px;\">";
#$page=preg_replace("/\"$tmpReplace\//","\"./",$page);
#$pageplots=preg_replace("/$tmpReplace\//","",$pageplots);
#$tmpReplace=preg_replace("/\//","\/",$baseURL);
#$page=preg_replace("/$tmpReplace\//","",$page);
#$pageplots=preg_replace("/$tmpReplace\//","",$pageplots);
$fp = fopen("$tmpDir/report.html","w");
#fwrite($fp,"<head><base href=\"$baseURL/\" target=\"_blank\"></head>");
fwrite($fp,"<head>");
fwrite($fp,"<link href=\"http://tesla.pcbi.upenn.edu/~pkuksa/SPAR/smdb-tab2.pdf.css\" rel=\"stylesheet\" type=\"text/css\">");
fwrite($fp,"</head>");
fwrite($fp,"<body>");
fwrite($fp,$page);
#fwrite($fp,$pageplots);
fwrite($fp,"</body>");
fclose($fp);
exec("/usr/local/bin/wkhtmltopdf --javascript-delay 2000 $tmpDir/report.html $tmpDir/report.pdf",$htmltopdf_out,$htmltopdf_ret_val);

# add report section / download
if ($htmltopdf_ret_val!=0)
  echo "<p>Conversion to PDF failed (Exit code=$htmltopdf_ret_val)";
else
{
  #echo "<h3>Report</h3>";
  /*
  echo "<div id=\"report\">";
  echo "<p><a href=\"$tmpDir/report.pdf\">Report (PDF)</a>";
  printFileSize("$tmpDir/report.pdf");
  echo "<p><a href=\"$tmpDir/report.html\">Report (HTML)</a>";
  printFileSize("$tmpDir/report.html");
  # add zip download
  echo "<p><a href=\"zipdownload.php?folder=$tmpDir\">Download results (ZIP)</a>";


  echo "</div>";
  */
}

echo "</div>"; // accordion

# add htaccess
exec("cp -p htaccess.template $tmpDir/results/.htaccess");
exec("cp -p htaccess.template $tmpDir/.htaccess");

# THIS IS A HACK
# replace peak tables with output files from R!!
exec("mv $tmpDir/results/Genomewide_distribution_patterns_of_small_RNA_loci_unannot_genomic_partition.txt $tmpDir/results/peaks_unannot.xls");
exec("mv $tmpDir/results/Genomewide_distribution_patterns_of_small_RNA_loci_annot_genomic_partition.txt $tmpDir/results/peaks_annot.xls");
#exec("mv $tmpDir/module3_annot.html $tmpDir/plots.html");
exec("rm $tmpDir/module3_annot.html");

# clean-up
if ($doCleanup==1)
{
  #exec("rm $tmpDir/*.sh");
  #exec("rm $tmpDir/*.r");
  exec("rm $tmpDir/raw.pos.*");
  exec("rm $tmpDir/raw.neg.*");
  exec("rm $tmpDir/*.stats");
  exec("mv $tmpDir/results.local.html $tmpDir/results.html");
  exec("mv $configFileSPAR $tmpDir/SPAR.$genomeType.config");
  exec("rm $tmpDir/*.sh $tmpDir/*.r");
  exec("rm $tmpDir/results/*.with_conservation*");
  #exec("rm $tmpDir/results/Genomewide*txt");
}

#echo "</div>";
/*
try
{
    $content="<page><body><p>Hello</p></body></page>";
    $html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(0, 0, 0, 0));
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->writeHTML($content);
    #$html2pdf->Output('test.pdf','D'); // force download pdf
    $html2pdf->Output("$tmpDir/test.pdf"); // display only
}
catch(HTML2PDF_exception $e) {
    echo $e;
    exit;
}

*/
#$html2pdf = new HTML2PDF("P", "A4", "en");
#$html2pdf->AddPage();
#$html2pdf->WriteHTML($page . $pageplots);
#$html2pdf->WriteHTML("<body>" . "<p>hello</p>" . "</body>");
#$html2pdf->Output("$tmpDir/report.pdf","F");

#https://genome.ucsc.edu/cgi-bin/hgTracks?db=hg18&position=chr21:33038447-33041505&hgct_customText=track%20type=bigBed%20name=myBigBedTrack%20description=%22a%20bigBed%20track%22%20visibility=full%20bigDataUrl=http://genome.ucsc.edu/goldenPath/help/examples/bigBedExample.bb
?>
</body>
