
<?php include "components/header-output.php";

@apache_setenv('no-gzip', 1);
@ini_set('zlib.output_compression', 0);
@ini_set('implicit_flush', 1);

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

ignore_user_abort(true);
// TODO: clean up run ID if time limit is exceeded
set_time_limit(7200); // not sure this is doing anything to the system calls..
session_cache_limiter('nocache');
session_start();
// set up javascript stuff
echo "<script type=\"text/javascript\">";
echo "function bgshowhide(id) { var e = document.getElementById(id); e.style.display = (e.style.display == 'block') ? 'none' : 'block'; }";
echo "</script>";

// make sure output buffering is off before we start it
// this will ensure same effect whether or not ob is enabled already
while (ob_get_level()) {
    ob_end_flush();
}
// start output buffering
if (ob_get_length() === false) {
    ob_start();
}

// print_r(array_values($_FILES));
// echo nl2br("\n");
// print_r(array_values($_POST));
echo "<div class=\"warning center\">";
if (empty($_FILES["fileToUpload"]["name"]) && $_POST['file_upload']=="NONE" && ($_POST["rsIDs"]=="rs1234\r\nrs5678" || empty($_POST["rsIDs"]))) {
    exit("No file was uploaded, no default was chosen, and no rsIDs were uploaded!!");
}
echo "<\div>";
// first check and parse all the optional arguments
$R2_thresh = $dist_thresh = $outprefix = "";
echo "<div class=\"parameters center\">";
if (!empty($_POST["R2_thresh"])) {
    $R2_thresh = test_input($_POST["R2_thresh"]);
    if(is_numeric($R2_thresh) && $R2_thresh > 0 && $R2_thresh < 1) {
        echo "R^2 threshold set to: " . "<span class=\"bold\">". $R2_thresh . "<\span>";
    } else {
        echo "R^2 threshold must be numeric and between 0 and 1! Using default value of 0.7" . nl2br("\n");
        echo "Submitted value: " . $R2_thresh;
        $R2_thresh = "0.7";
     }
} else {
    $R2_thresh = "0.7";
    echo "R^2 threshold set to: " . "<span class=\"bold\">" . $R2_thresh . "<\span>";
}
echo "<\div>";
echo "<div class=\"parameters center\">";
if (!empty($_POST["dist_thresh"])) {
    $dist_thresh = test_input($_POST["dist_thresh"]);
    if (!preg_match("/^[0-9]*$/", $dist_thresh)) {
        echo "Distance threshold must be numeric! Using default value of 500000" . nl2br("\n");
        echo "Submitted value: " . $dist_thresh;
        $dist_thresh = "500000";
    }
    else {
        echo "Distance threshold set to: " . "<span class=\"bold\">" . $dist_thresh . "<\span>";
    }
} else {
    $dist_thresh = "500000";
    echo "Distance threshold set to: " . "<span class=\"bold\">" . $dist_thresh . "<\span>";
}
echo "<\div>";
// assign a unique identifier to this run and make a new folder
$this_id = substr(md5(rand()), 0, 7);
// $this_id = "Nalls_PD";
echo "<div class=\"parameters center\">";
echo "Identifier is:" . "<span class=\"identifier bold\">" . $this_id . "</span>";
echo "<\div>";

// ??????????????????????????????????????????????????????????????????????????????????????????????????????????
// now define the outprefix, using this if necessary
if (!empty($_POST["outprefix"])) {
    $outprefix = test_input($_POST["outprefix"]);
    echo "Prefix for output files set to " . $outprefix . nl2br("\n");
} else {
    $outprefix = $this_id;
}

$target_dir = "/mnt/data/INFERNO/output/user_data/" . $this_id . "/";
$full_log_file = $target_dir . "/" . $this_id . "_full_pipeline_log.txt";
// set up file upload / copying
mkdir($target_dir);
shell_exec("chmod a+wrx $target_dir");

// top priority is user input
ob_flush();
flush();
// ??????????????????????????????????????????????????????????????????????????????????????????????????????????

// include "/var/www/html/INFERNO_output_header.html";

echo "<div id=\"results\">";

echo "<h2 class=\"subtitle\" data-collapse-summary>Run Log <span class=\"collapse-icon\"><i class=\"fa fa-plus\" aria-hidden=\"true\"></i><i class=\"fa fa-minus\" aria-hidden=\"true\"></i></span></h2>";
echo "<div class=\"close\">";
// echo json_encode(htmlspecialchars($_POST['rsIDs']));
if (!empty($_FILES["fileToUpload"]["name"])) {
    // don't allow anything more than 10Mb
    if ($_FILES["fileToUpload"]["size"] > 10000000) {
        echo "Too large";
        exit("Sorry, your file is too large (must be less than 10Mb).");
    }

    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);

    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
    } else {
        exit("Sorry, there was an error uploading your file.");
    }
} elseif (!(substr($_POST["rsIDs"], 0, 6)=="rs1234" || empty($_POST["rsIDs"]))) {
    // then try to parse custom rsIDs
    $target_file = $target_dir . $outprefix . "_custom_rsIDs.INFERNO_input.txt";
    echo "Looking up input rsIDs.. ";
    ob_flush();
    flush();
    system('bash /var/www/html/lookup_user_rsIDs.sh ' . $target_file . ' ' . str_replace(array("\r", "\n"), " ", strip_tags($_POST['rsIDs'])));

    $num_found = exec("cat " . $target_file . " | wc -l");
    if($num_found == "0") {
        exit("Sorry, none of the input rsIDs were found in our database!");
    }
} else {
    // if we're using a default file, parse that
    $target_file = $target_dir . basename($_POST['file_upload']);
    // basically we just have to copy the file from the default place to this new directory
    copy($_POST['file_upload'], $target_file);
}

$file_extension = pathinfo($target_file,PATHINFO_EXTENSION);
ob_flush();
echo nl2br("\n");

// check that the file is in INFERNO format
echo nl2br(passthru('bash /var/www/html/check_INFERNO_input.sh ' . $target_file . ' 2>&1', $format_check_return));
if ($format_check_return != 0) {
    echo nl2br("\n");
    ob_flush();
    flush();
    exit("Format check failed!");
}

// copy and edit the configuration file
$new_cfg = $target_dir . '/' . $this_id . '_config_file.cfg';
copy("/home/alexamlie/code/INFERNO/param_files/INFERNO_web_server_config.cfg", $new_cfg);
exec('sed -i "s/LD_THRESH=/LD_THRESH=' . $R2_thresh . '/" ' . $new_cfg);
exec('sed -i "s/LD_AREA=/LD_AREA=' . $dist_thresh . '/" ' . $new_cfg);

// run through the pipeline
$INFERNO_cmd = 'bash /var/www/html/INFERNO.server.sh ' . $target_file . ' ' . $new_cfg . ' ' . $target_dir . ' ' . $outprefix . " 2>&1 | tee -a " . $full_log_file;
// $INFERNO_cmd = 'echo $PATH; sleep 10s; echo "hi"; sleep 10s; echo "hi"; sleep 10s; echo "hi";';
echo $INFERNO_cmd;
echo nl2br("\n");

// move to the directory
chdir("/home/alexamlie/code/INFERNO/");

echo nl2br("\n");
// add this job to the file containing all currently running IDs
$id_file = "/mnt/data/INFERNO/output/user_data/cur_run_IDs.txt";
file_put_contents($id_file, $this_id . "\n", FILE_APPEND | LOCK_EX);
// also add it to a file with the master list of run IDs
file_put_contents("/mnt/data/INFERNO/output/user_data/all_run_IDs.txt", $this_id . "\t" . date("Y-m-d\th:i:sa") . "\t" . $outprefix . "\t" . $_SERVER['REMOTE_ADDR'] . "\n", FILE_APPEND | LOCK_EX);

// now only run the pipeline if this is the first ID in that file
$cur_first_id = exec("head -1 " . $id_file);
$num_files = exec("cat " . $id_file . " | wc -l");
ob_flush();
while($cur_first_id != $this_id) {
    echo "Waiting for " . ($num_files-1) . " other pipeline runs to finish.. ".date("h:i:sa")."\n";
    ob_flush();
    flush();
    sleep(30);
    $cur_first_id = exec("head -1 " . $id_file);
    $num_files = exec("wc -l " . $id_file);
}

echo nl2br("\n");
echo 'Running pipeline';
echo nl2br("\n");
ob_flush();
flush();
// while(@ ob_end_flush());
echo "<pre>";
$INFERNO_proc = popen($INFERNO_cmd, 'r');
while (!feof($INFERNO_proc))
{
    // $s=fgets($INFERNO_proc);
    // echo "<p>$s\n";
    echo fread($INFERNO_proc, 1024);
    ob_flush();
    flush();
}
echo "</pre>";
pclose($INFERNO_proc);
echo nl2br("\n");
ob_flush();
flush();

echo nl2br("Processing and compressing results\n");

// remove this ID from the run ID file
exec("grep -v " . $this_id . " " . $id_file . " > " . $id_file . "_temp");
exec("mv " . $id_file . "_temp " . $id_file);

// compress the results
$compressed_out_file = "/mnt/data/INFERNO/output/user_data/" . $outprefix . "_results.tar.gz";
$compression_out = shell_exec("cd /mnt/data/INFERNO/output/user_data/; tar -czf " . $compressed_out_file  . " " . $this_id . "/; mv " . $compressed_out_file . " " . $this_id . "/");

// save the annotation count summary file
$summary_path = "user_data/" . $this_id . "/" . $outprefix . "_annotation_overlap_summary.txt";
shell_exec("cp /mnt/data/INFERNO/output/user_data/" . $this_id . "/summaries/" . $outprefix . "_annotation_overlap_summary.txt /mnt/data/INFERNO/output/" . $summary_path);

// remove other files besides plots
echo nl2br("Cleaning up output\n");
// first start with the top level directories
$dirs_to_remove = array("targetscan_miRNA_overlap", "fantom5_overlap", "unstranded_genomic_partition", "closest_gene", "dashr_ncrna_loci_overlap", "gtex_eqtl_overlap", "roadmap_chromhmm_states", "closest_fantom5_enh", "factorbook_overlap", "correlation_enh_targets", "summaries", "homer_motif_overlap", "ld_expansion");
foreach ($dirs_to_remove as $rm_dir) {
    echo nl2br("Cleaning up directory " . $rm_dir . "\n");
    $file_remove_out = shell_exec("rm -rf /mnt/data/INFERNO/output/user_data/" . $this_id . "/" . $rm_dir);
}
// now remove the tables in the analysis results
$analysis_dirs = array("closest_fantom5_enhs", "dashr_ncrna_loci_overlap", "factorbook_overlap", "fantom5_eqtl_chromHMM_overlap", "fantom5_overlap", "fantom5_roadmap_overlap", "gtex_eqtl_overlap", "homer_motif_overlap", "ld_stats", "roadmap_chromHMM", "targetscan_miRNA_overlap", "unstranded_genomic_partition");
foreach ($analysis_dirs as $rm_dir) {
    echo nl2br("Cleaning up analysis directory " . $rm_dir . "\n");
    $file_remove_out = shell_exec("rm -rf /mnt/data/INFERNO/output/user_data/" . $this_id . "/analysis_results/" . $rm_dir . "/tables/");
}

// allow users to hide the log file
// echo "<p><a href=\"javascript:bgshowhide('full_log_file')\">INFERNO run log</a>";



// ??????????????????????????????????????????????????????????????????????????????????????


echo "<div id=\"full_log_file\" style=\"display:none;width:100%\">";
echo "<pre>";
include "$full_log_file";
echo "</pre>";
echo "</div>";

echo "</div>";  // .close runLog

ob_flush();
flush();

// show summary of output
echo "<h2 id=\"summary\" class=\"subtitle\" data-collapse-summary>Summary of results <span class=\"collapse-icon\"><i class=\"fa fa-plus\" aria-hidden=\"true\"></i><i class=\"fa fa-minus\" aria-hidden=\"true\"></i></span></h2>";
echo "<div class=\"close\">";
// pull out the summary script output of number of SNPs
$snp_count_output = shell_exec('grep -A5 "unique SNPs" ' . $full_log_file);
echo nl2br($snp_count_output);

echo "</div>"; // .close summary

echo "<h2 class=\"subtitle\" data-collapse-summary >Download Results <span class=\"collapse-icon\"><i class=\"fa fa-plus\" aria-hidden=\"true\"></i><i class=\"fa fa-minus\" aria-hidden=\"true\"></i></span></h2>";
echo "<div class=\"close\">";
$output_path = "user_data/" . $this_id . "/";
$plot_path = "user_data/" . $this_id . "/" . $outprefix . "_plots.html";
echo "<p>Link to results directory: <a href=\"$output_path\" target=\"_blank\">$this_id</a> (will be kept for 1 week). Note that this directory only contains logs and plots; the full results are in the tar.gz file</p>";
echo "<p>Link to annotation overlap summary file: <a href=\"$summary_path\" target=\"_blank\">$this_id</a> (will be kept for 1 week)</p>";
echo "<p>Link to stand-alone result plot HTML page: <a href=\"$plot_path\" target=\"_blank\">$this_id</a> (will be kept for 1 week)</p>";
$tarball_path = "user_data/" . $this_id . "/" . $outprefix . "_results.tar.gz";
echo "<p>Link to tarball of full results: <a href=\"$tarball_path\">Download results (tar.gz)</a> (will be kept for 1 week)</p>";
echo "</div>"; // .close download

// show the plots
echo "<h2 class=\"subtitle\" data-collapse-summary >Plots <span class=\"collapse-icon\"><i class=\"fa fa-plus\" aria-hidden=\"true\"></i><i class=\"fa fa-minus\" aria-hidden=\"true\"></i></span></h2>";
echo "<div class=\"close\">";
// $page = ob_get_contents();
// echo "<p>Hover to preview plots, click to see hi-res image in the new window</p>";
// copy and edit the plot template for this run
$sed_cmd = "sed -e 's/RUN_ID/" . $this_id . "/g' /var/www/html/plot_output_template.html | sed -e 's/OUTPREFIX/" . $outprefix . "/g' | sed -e 's/DIST/" . $dist_thresh . "/g' | sed -e 's/R2/" . $R2_thresh . "/g' > /mnt/data/INFERNO/output/" . $plot_path;
$template_edit = shell_exec($sed_cmd);

include $plot_path;
echo "</div>"; // .close plots
echo "</div>"; // #results

// var_dump(ob_get_clean());
?>
<script charset="utf-8">
(function($) {

    $("#results").collapse({
      open: function() {
        this.slideDown(250);
      },
      close: function() {
        this.slideUp(250);
      }
    });
})( jQuery );
</script>

<?php
include "components/footer-output.php"; ?>
