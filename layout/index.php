<?php include "components/header.php"; ?>
  <div class="content home">
    <section class="top-boxes">
    </section>

    <main class="main-area">

      <div class="text">
        <form action="analyze_top_snps.php" method="POST" enctype="multipart/form-data">
          <div class="form-part">
            <h3>Select a default GWAS for analysis:</h3>
            <select name="file_upload" autocomplete="off">
              <option value="NONE" selected="selected">Select a GWAS</option>
              <option value="default_inputs/SCZ2_128_top_variants_INFERNO_input.no_chrX.txt">
                PGC Schizophrenia Analysis</option>
              <option value="default_inputs/PD_top_variants_INFERNO_input.no_missing.tsv">
                Parkinson's Disease 2014 GWAS, Nalls et al.</option>
            </select>
          </div><!-- .form-part-->

          <div class="form-part">
          	<h3>Or upload a tab separated file (columns: chromosome \t rsID \t region name \t position, maximum of 8Mb):</h3>
      	    <span class="choose">
      	       <input type="file" name="fileToUpload" id="fileToUpload" class="inputfile" />
               <label for="fileToUpload"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg><span>Choose a file</span></label>
            </span>
            </div><!-- .form-part-->
            <div class="form-part">
              <h3>Or enter your rsIDs of interest here:</h3>
              <div id="text-area-wrap">
                <textarea id="text-area" name="rsIDs" rows="5"></textarea>
                <div id="placeholder">rs1234 <br>rs5678</div>
              </div>
            </div>

          <div class="form-part">
    	      <h3>Optional parameters:</h3>
    	      <div class="pmt">
      	      Threshold on R^2: <input type="text" name="R2_thresh">
            </div>
            <div class="pmt">
    	         Threshold on LD block size: <input type="text" name="dist_thresh">
    	      </div>
            <div class="pmt">
    	         Prefix for output files: <input type="text" name="outprefix">
            </div>
          </div><!-- .form-part-->

    	    <input type="submit" value="Upload file and parameters" name="submit">
        </form>
      </div><!-- .text -->

      <div class="important">
      <h4>IMPORTANT:</h4>
      INFERNO analysis typically takes around 15 minutes, if you close your browser
      before that, you can access your results once they're done at
      <span class="break">http://inferno.lisanwanglab.org/user_data/ID/</span> if you replace ID with the identifier
      for your run.
      </div>

      <div class="text">
        <strong>Or, view processed plot outputs for the default GWASs:</strong>
        <ul>
          <li><a href="../user_data/PGC_SCZ2/PGC_SCZ2_plots.html" target="_blank">PGC Schizophrenia</a></li>
          <li><a href="../user_data/Nalls_PD/Nalls_PD_plots.html" target="_blank">Parkinson's Disease 2014 GWAS, Nalls et al.</a></li>
        </ul>
      </div><!-- .text -->
    </main><!-- .main-area -->
  </div><!-- .content -->


  <?php include "components/footer.php"; ?>
