<!DOCTYPE html>
<html lang="en-US">

<head>
  <meta charset="utf-8">
  <title>INFERNO - INFERring the molecular mechanisms of NOncoding genetic variants</title>
  <!-- <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="Description" lang="en" content="INFERNO - INFERring the molecular mechanisms of NOncoding genetic variants">
  <meta name="author" content="Alexandre Amlie-Wolf, Ph.D. Student, Genomics and Computational Biology, University of Pennsylvania">
  <meta name="robots" content="index, follow">


  <!-- Stylesheets, inlcuding custom fonts, go here -->
  <link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css" type="text/css" media="all">
  <link rel="stylesheet" href="style.css" type="text/css" media="all">

  <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js">></script>
  <script type="text/javascript" src="js/libs/jquery.collapse.js"></script>
</head>

<body>
  <a class="skip-link screen-reader-text" href="#content">Skip to content</a>

  <header class="masthead">
    <div class="site-branding">
      <a href="index.php">
        <figure class="logo"><img src="img/inferno_logo.svg" alt="Inferno"></figure>
        <p class="site-title">INFERring the molecular mechanisms of NOncoding genetic variants</p>
      </a>
    </div><!-- .site-title -->
    <nav id="site-navigation" class="main-navigation" role="navigation">
      <!-- Mobile menu toggle button -->
      <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
      </button>
      <!-- Main Menu -->
      <div class="menu-main-menu-container">
        <ul id="primary-menu" class="nav-menu">
          <!-- <li class="menu-item current-menu-item"><a href="index.html">Home</a></li> -->
          <!-- <li class="menu-item menu-item-has-children"><a href="#">Item with sub-items</a>
              <ul class="sub-menu">
                  <li class="menu-item"><a href="#">Sub-item One</a></li>
                  <li class="menu-item"><a href="#">Sub-item Two</a></li>
                  <li class="menu-item"><a href="#">Sub-item Three</a></li>
              </ul>
          </li> -->
          <li class="menu-item"><a href="index.php">Run INFERNO</a></li>
          <li class="menu-item"><a href="description.php">Description and Citation</a></li>
          <li class="menu-item"><a href="documentation.php">Documentation and ReadMe</a></li>
          <li class="menu-item"><a href="download.php">Download INFERNO</a></li>

        </ul>
      </div><!-- .menu-main-menu-container -->
    </nav>
  </header><!-- .masthead -->
