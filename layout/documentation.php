<?php include "components/header.php"; ?>
  <div class="content">
    <main class="main-area">
      <h1>Documentation &amp; README</h1>
      <div class="text">
        <h2>Method description:</h2>
        <p>The majority of variants identified by genome-wide association studies (GWAS) reside in the noncoding genome, where they affect regulatory elements including transcriptional enhancers. We propose INFERNO (INFERring the molecular mechanisms of NOncoding genetic variants), a novel method which integrates hundreds of diverse functional genomics data sources with GWAS summary statistics to identify putatively causal noncoding variants underlying association signals. INFERNO comprehensively characterizes the relevant tissue contexts, target genes, and downstream biological processes affected by functional variants.</p>
      </div><!-- .text -->
      <div class="text">
        <h2>Instructions for use:</h2>

        <h3>Obtaining source code and annotations:</h3>
        <p>To use INFERNO, pull the source code from <a href="https://bitbucket.org/wanglab-upenn/INFERNO" target="_blank">the bitbucket repository</a>. The full processed annotation datasets used for the tool are <a href="https://www.lisanwanglab.org/~alexaml/full_INFERNO_annotations.tar.gz">available for
download</a>. There is also a <a href="http://inferno.lisanwanglab.org">web server</a> that runs a subset of the INFERNO analyses. The full INFERNO pipeline can be run on a bsub-based cluster system (default) or through direct execution, using the '--cluster_system' flag. To extract the annotation data and set up the configuration file, run these steps:</p>
        <pre><span class="comment">## start from the directory containing full_INFERNO_annotations.tar.gz</span><br>$ tar -xzvf full_INFERNO_annotations.tar.gz<br>$ <span class="nb">cd</span> full_INFERNO_annotations/<br>$ ./update_config_file.sh
        </pre>

        <h3>Options in configuration file:</h3>
        <p>To use the INFERNO pipeline, some arguments are given to the command line tool (see below), and some are set up in the configuration file, mostly for setting up where various data files are:</p>
        <table class="documentation-table">
          <thead>
            <tr>
              <th>Config file variable</th>
              <th>Value</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><strong>Parameters for annotation overlaps</strong></td>
              <td></td>
            </tr>
            <tr>
              <td>KG_POP</td>
              <td>Desired 1,000 genomes population to use (EUR, AMR, ASN, AFR), EUR by default</td>
            </tr>
            <tr>
              <td>LD_THRESH</td>
              <td>Threshold for R^2 values of the LD expansion (Default = 0.7)</td>
            </tr>
            <tr>
              <td>LD_AREA</td>
              <td>Distance around each tag variant to check (Default = 500000)</td>
            </tr>
            <tr>
              <td>KG_DIR</td>
              <td>The folder containing the sorted 1,000 genomes vcf files</td>
            </tr>
            <tr>
              <td>GENE_BED_FILE</td>
              <td>Bed file containing exons of protein-coding genes</td>
            </tr>
            <tr>
              <td>KGXREF_FILE</td>
              <td>Reference file to match exon IDs to gene names</td>
            </tr>
            <tr>
              <td>UNSTRANDED_PARTITION_DIR</td>
              <td>Directory containing parsed partition information</td>
            </tr>
            <tr>
              <td>FANTOM5_DIR</td>
              <td>Directory containing sorted FANTOM5 facet-level enhancer bed files</td>
            </tr>
            <tr>
              <td>ENHANCER_LOCUS_WINDOW</td>
              <td>Base-pair window around FANTOM5 enhancer loci to look for overlaps (Default = 1000)</td>
            </tr>
            <tr>
              <td>FANTOM5_CORRELATION_FILE</td>
              <td>File containing the correlation-based TSS targets of FANTOM5 enhancers</td>
            </tr>
            <tr>
              <td>ROADMAP_CHROMHMM_DIR</td>
              <td>Directory containing the sorted bed files of ChromHMM states across Roadmap samples</td>
            </tr>
            <tr>
              <td>BEDTOOLS_BIN_DIR</td>
              <td>Specific path to bedtools, or can be left undefined if you have bedtools in your path</td>
            </tr>
            <tr>
              <td>GTEX_DIR</td>
              <td>Directory containing the sorted files of significant eQTL signals (for direct overlap)</td>
            </tr>
            <tr>
              <td>FACTORBOOK_FILE</td>
              <td>If desired, the file containing the FactorBook TFBS annotations</td>
            </tr>
            <tr>
              <td>HOMER_MOTIF_BED_FILE</td>
              <td>The bed file containing the positions of HOMER-identified TFBSs</td>
            </tr>
            <tr>
              <td>HOMER_MOTIF_PWM_FILE</td>
              <td>The custom.motifs file containing the PWMs for each TF analyzed by HOMER</td>
            </tr>
            <tr>
              <td>HOMER_MOTIF_SEQ_FILE</td>
              <td>The processed file containing the sequences of each HOMER TFBS, for delta PWM calculation</td>
            </tr>
            <tr>
              <td>DASHR_LOCUS_FILE</td>
              <td>The path to the BED file containing DASHR small noncoding RNA loci</td>
            </tr>
            <tr>
              <td>TARGETSCAN_DIR</td>
              <td>The path to the directory containing TargetScan miRNA binding site predictions</td>
            </tr>
            <tr>
              <td>F5_CLASSES</td>
              <td>The text file containing the FANTOM5 tissue class assignments</td>
            </tr>
            <tr>
              <td>GTEX_CLASSES</td>
              <td>The text file containing the GTEx tissue class assignments</td>
            </tr>
            <tr>
              <td>ROADMAP_CLASSES</td>
              <td>The text file containing the Roadmap tissue class assignments</td>
            </tr>
            <tr>
              <td><strong>Parameters for enhancer enrichment analysis</strong></td>
              <td></td>
            </tr>
            <tr>
              <td>NUM_SAMPLES</td>
              <td>The number of control variant sets to sample (Default = 10,000)</td>
            </tr>
            <tr>
              <td>MAF_BIN_SIZE</td>
              <td>The size of the bins to group variants by minor allele frequency (Default = 0.01)</td>
            </tr>
            <tr>
              <td>DIST_ROUND</td>
              <td>The rounding constant / bin size for distance to the nearest TSS (Default = 1000)</td>
            </tr>
            <tr>
              <td>DIST_THRESHOLD</td>
              <td>An upper threshold on the distance to the nearest TSS, after which all variants are grouped together as 'high' (Default = "Inf" -&gt; no limit)</td>
            </tr>
            <tr>
              <td>LD_PARTNER_THRESHOLD</td>
              <td>An upper threshold on the number of LD partners, after which all variants are grouped together as 'high' (Default = "Inf" -&gt; no limit)</td>
            </tr>
            <tr>
              <td>BG_SNP_INFOF</td>
              <td>The file containing the sampling characteristics for all variants (see below for generation)</td>
            </tr>
            <tr>
              <td>LD_SETS_DIR</td>
              <td>The directory containing the precomputed sets of LD blocks for all variants</td>
            </tr>
            <tr>
              <td>REF_SUMMARY_DIR</td>
              <td>The directory containing the summary files of annotation overlaps for all 1,000 genomes variants</td>
            </tr>
            <tr>
              <td><strong>Parameters for co-localization analysis</strong></td>
              <td></td>
            </tr>
            <tr>
              <td>COLOC_H4_THRESH</td>
              <td>The threshold on P(H_4) to define a strong colocalization signal (Default = 0.5)</td>
            </tr>
            <tr>
              <td>COLOC_ABF_THRESH</td>
              <td>The threshold on the amount of cumulative ABF density that should be accounted for by expansion (Default = 0.5)</td>
            </tr>
            <tr>
              <td>LOCUSZOOM_PATH</td>
              <td>If desired, the direct path to the locusZoom executable to generate GWAS and eQTL locusZoom plots for strong colocalization signals</td>
            </tr>
            <tr>
              <td>COLOC_GTEX_DIR</td>
              <td>The directory containing the full GTEx dataset (not just the significant eQTLs, but all associations)</td>
            </tr>
            <tr>
              <td>GTEX_SAMPLE_SIZEF</td>
              <td>The csv containing the GTEx sample sizes for each eQTL dataset, for COLOC</td>
            </tr>
            <tr>
              <td>GTEX_RSID_MATCH</td>
              <td>The file that is used to cross-reference GTEx IDs with rsIDs</td>
            </tr>
            <tr>
              <td>HG19_ENSEMBL_REF_FILE</td>
              <td>The file that is used to cross-reference Ensembl gene IDs with gene names</td>
            </tr>
            <tr>
              <td>RELEVANT_CLASSES</td>
              <td>The set of tissue categories that you especially care about, if any. There must be no spaces in this, or the script breaks! (Default = <span class="break">"'Blood','Brain','Connective Tissue'")</span>. Follow the formatting of the default argument to make sure it gets parsed correctly</td>
            </tr>
            <tr>
              <td><strong>Parameters for lncRNA correlation analysis</strong></td>
              <td></td>
            </tr>
            <tr>
              <td>GTEX_EXPR_DIR</td>
              <td>The directory containing the parsed RNAseq data (see below)</td>
            </tr>
            <tr>
              <td>SAMPLE_INFO_FILE</td>
              <td>The file containing the GTEx sample attributes to match IDs with tissues</td>
            </tr>
            <tr>
              <td>GENCODE_LNCRNA_FILE</td>
              <td>The file containing the GENCODE lncRNA annotations, used to detect lncRNA eQTL targets</td>
            </tr>
            <tr>
              <td>COR_THRESH</td>
              <td>The absolute value threshold on Pearson and Spearman correlation to define strong lncRNA targets (Default = 0.5)</td>
            </tr>
          </tbody>
        </table>

        <h3>Dependencies and requirements</h3>
        <p>To run the full pipeline, around 40Gb of memory is required, as the enhancer sampling, co-localization, and lncRNA correlation analyses are computationally intensive and run in R, which loads objects into memory. A typical run for the full pipeline will generate around 10-20Gb of result data. The <a href="http://tesla.pcbi.upenn.edu/~alexaml/INFERNO/full_INFERNO_annotations.tar.gz">full annotation data</a> is around 130Gb compressed and expands to around 320Gb of annotation data.</p>
        <p>To run the pipeline, the following main programs are required:</p>
        <ul>
          <li>Python v2.7.9 or a higher version of Python 2.7</li>
          <li>bedtools v2.25.0 or greater version of bedtools v2</li>
          <li>R 3.2.3</li>
          <li>plink v1.90b2i or greater</li>
        </ul>
        <p>Additionally, the following packages are required:</p>
        <p><strong>For Python:</strong></p>
        <ul>
          <li>argparse</li>
          <li>subprocess</li>
          <li>datetime</li>
          <li>os</li>
          <li>time</li>
          <li>sys</li>
          <li>glob</li>
          <li>gzip</li>
          <li>re</li>
          <li>subprocess</li>
          <li>copy</li>
          <li>commands</li>
          <li>pickle</li>
          <li>math</li>
        </ul>
        <p><strong>For R:</strong></p>
        <ul>
          <li>data.table</li>
          <li>ggplot2</li>
          <li>gplots</li>
          <li>gtools</li>
          <li>plyr</li>
          <li>psych</li>
          <li>coloc</li>
          <li>RColorBrewer</li>
          <li>reshape2</li>
          <li>scales</li>
        </ul>
        <p>Here is the output of a sessionInfo() call for R package versions used to develop the pipeline:</p>
        <div class="language-R">
          <pre>
<span class="o">&gt;</span> sessionInfo<span class="p">()</span>
R version <span class="m">3.2.3</span> <span class="p">(</span><span class="m">2015-12-10</span><span class="p">)</span><br>
Platform<span class="o">:</span> x86_64<span class="o">-</span>pc<span class="o">-</span>linux<span class="o">-</span>gnu <span class="p">(</span><span class="m">64</span><span class="o">-</span>bit<span class="p">)</span>
Running under<span class="o">:</span> CentOS release <span class="m">6.9</span> <span class="p">(</span>Final<span class="p">)</span>

locale<span class="o">:</span>
<span class="p">[</span><span class="m">1</span><span class="p">]</span> LC_CTYPE<span class="o">=</span>en_US.UTF<span class="m">-8</span>       LC_NUMERIC<span class="o">=</span>C               LC_TIME<span class="o">=</span>en_US.UTF<span class="m">-8</span>
<span class="p">[</span><span class="m">4</span><span class="p">]</span> LC_COLLATE<span class="o">=</span>en_US.UTF<span class="m">-8</span>     LC_MONETARY<span class="o">=</span>en_US.UTF<span class="m">-8</span>    LC_MESSAGES<span class="o">=</span>en_US.UTF<span class="m">-8</span>
<span class="p">[</span><span class="m">7</span><span class="p">]</span> LC_PAPER<span class="o">=</span>en_US.UTF<span class="m">-8</span>       LC_NAME<span class="o">=</span>C                  LC_ADDRESS<span class="o">=</span>C
<span class="p">[</span><span class="m">10</span><span class="p">]</span> LC_TELEPHONE<span class="o">=</span>C             LC_MEASUREMENT<span class="o">=</span>en_US.UTF<span class="m">-8</span> LC_IDENTIFICATION<span class="o">=</span>C

attached base packages<span class="o">:</span>
<span class="p">[</span><span class="m">1</span><span class="p">]</span> stats     graphics  grDevices utils     datasets  methods   base

other attached packages<span class="o">:</span>
<span class="p">[</span><span class="m">1</span><span class="p">]</span> psych_1.7.3.21     coloc_2.3<span class="m">-1</span>        BMA_3.18.6         rrcov_1.4<span class="m">-3</span>
<span class="p">[</span><span class="m">5</span><span class="p">]</span> inline_0.3.14      robustbase_0.92<span class="m">-7</span>  leaps_3.0          survival_2.41<span class="m">-3</span>
<span class="p">[</span><span class="m">9</span><span class="p">]</span> MASS_7.3<span class="m">-45</span>        colorspace_1.3<span class="m">-2</span>   gtools_3.5.0       RColorBrewer_1.1<span class="m">-2</span>
<span class="p">[</span><span class="m">13</span><span class="p">]</span> gplots_3.0.1       reshape2_1.4.2     scales_0.4.1       plyr_1.8.4
<span class="p">[</span><span class="m">17</span><span class="p">]</span> ggplot2_2.2.1      data.table_1.10.4

loaded via a namespace <span class="p">(</span>and not attached<span class="p">)</span><span class="o">:</span>
<span class="p">[</span><span class="m">1</span><span class="p">]</span> pcaPP_1.9<span class="m">-61</span>       Rcpp_0.12.10       compiler_3.2.3     DEoptimR_1.0<span class="m">-8</span>
<span class="p">[</span><span class="m">5</span><span class="p">]</span> bitops_1.0<span class="m">-6</span>       tools_3.2.3        nlme_3.1<span class="m">-131</span>       tibble_1.3.4
<span class="p">[</span><span class="m">9</span><span class="p">]</span> gtable_0.2.0       lattice_0.20<span class="m">-35</span>    rlang_0.1.2        Matrix_1.2<span class="m">-11</span>
<span class="p">[</span><span class="m">13</span><span class="p">]</span> parallel_3.2.3     mvtnorm_1.0<span class="m">-6</span>      stringr_1.2.0      cluster_2.0.6
<span class="p">[</span><span class="m">17</span><span class="p">]</span> caTools_1.17.1     stats4_3.2.3       grid_3.2.3         foreign_0.8<span class="m">-67</span>
<span class="p">[</span><span class="m">21</span><span class="p">]</span> gdata_2.17.0       magrittr_1.5       splines_3.2.3      mnormt_1.5<span class="m">-5</span>
<span class="p">[</span><span class="m">25</span><span class="p">]</span> KernSmooth_2.23<span class="m">-15</span> stringi_1.1.5      lazyeval_0.2.0     munsell_0.4.3
          </pre>
        </div>

        <h3>Running the tool</h3>
        <p>Then, there are two ways to run the pipeline. The more general approach is to make sure that python, bedtools, R, and plink are in your path. With this approach, you can run the full INFERNO pipeline using the INFERNO.py script:
        </p>
        <div class="language-bash">
          <pre><span></span>$ python INFERNO.py -h
usage: INFERNO.py <span class="o">[</span>-h<span class="o">]</span> <span class="o">[</span>--skip_ld_expansion<span class="o">]</span> <span class="o">[</span>--rsid_column RSID_COLUMN<span class="o">]</span> <span class="o">[</span>--pos_column POS_COLUMN<span class="o">]</span> <span class="o">[</span>--pval_column PVAL_COLUMN<span class="o">]</span> <span class="o">[</span>--chr_column CHR_COLUMN<span class="o">]</span>
                  <span class="o">[</span>--allele1_column ALLELE1_COLUMN<span class="o">]</span> <span class="o">[</span>--allele2_column ALLELE2_COLUMN<span class="o">]</span> <span class="o">[</span>--maf_column MAF_COLUMN<span class="o">]</span> <span class="o">[</span>--beta_column BETA_COLUMN<span class="o">]</span> <span class="o">[</span>--run_pval_expansion<span class="o">]</span>
                  <span class="o">[</span>--sig_mult SIG_MULT<span class="o">]</span> <span class="o">[</span>--case_prop CASE_PROP<span class="o">]</span> <span class="o">[</span>--sample_size SAMPLE_SIZE<span class="o">]</span> <span class="o">[</span>--summary_file SUMMARY_FILE<span class="o">]</span> <span class="o">[</span>--run_enhancer_sampling<span class="o">]</span> <span class="o">[</span>--run_gtex_coloc<span class="o">]</span>
                  <span class="o">[</span>--run_lncrna_correlation<span class="o">]</span>
                  top_snpf cfg_file outdir outprefix

Driver script <span class="k">for</span> the INFERNO pipeline

positional arguments:
top_snpf              The tab separated file of the tag SNPs you want to analyze. Should be formatted with four columns:
                      chromosome, rsID, region naming information, and position <span class="o">(</span>in that order<span class="o">)</span>.
                      IMPORTANT: Note that SNPs without dbSNP rsIDs should use <span class="s1">'chr-pos'</span> naming format, not <span class="s1">'chr:pos'</span>, which is incompatible with this pipeline!
cfg_file              The configuration file containing paths to all the required functional annotation files. Should be formatted as a bash configuration file i.e.
                      <span class="nv">VARIABLE</span><span class="o">=</span>DEFINTIION on each line.
outdir                The directory to write all the results to.
outprefix             The desired prefix <span class="k">for</span> all the output file names

optional arguments:
-h, --help            show this <span class="nb">help</span> message and <span class="nb">exit</span>
--skip_ld_expansion   Give this flag to skip any LD <span class="o">(</span>and p-value-based<span class="o">)</span> expansion and just run analysis directly on the provided list of input variants.
--rsid_column RSID_COLUMN
                      The summary statistics column number containing the rsIDs
--pos_column POS_COLUMN
                      The summary statistics column number containing the positions
--pval_column PVAL_COLUMN
                      The summary statistics column number containing the p-values
--chr_column CHR_COLUMN
                      The summary statistics column number containing the chromosomes
--allele1_column ALLELE1_COLUMN
                      The summary statistics column number containing allele <span class="m">1</span>, which should correspond to the major allele.
--allele2_column ALLELE2_COLUMN
                      The summary statistics column number containing allele <span class="m">2</span>, which should correspond to the minor allele.
--maf_column MAF_COLUMN
                      The summary statistics column number containing the minor allele frequency. Note that <span class="k">if</span> this is ever greater than <span class="m">0</span>.5 and a beta column is provided,
                      the effect direction will be flipped to be defined relative to the minor allele.
--beta_column BETA_COLUMN
                      The summary statistics column number containing the beta estimate <span class="o">(</span>used <span class="k">for</span> p-value expansion with consistent directions<span class="o">)</span>
--run_pval_expansion  If you want to <span class="k">do</span> expansion by p-values when you have summary statistics, provide this flag. Otherwise, the top SNP file will be directly expanded.
--sig_mult SIG_MULT   The multiplier range <span class="k">for</span> significance of the p-value expanded variant <span class="nb">set</span>
                      <span class="o">(</span>e.g. a value of <span class="m">10</span> means one order of magnitude<span class="o">)</span>
--case_prop CASE_PROP
                      The proportion of cases in the GWAS, <span class="k">for</span> colocalization.
--sample_size SAMPLE_SIZE
                      The total number of samples in the GWAS, <span class="k">for</span> colocalization.
--summary_file SUMMARY_FILE
                      The path to the full summary statistics file, required <span class="k">for</span> p-value expansion, colocalization analysis, and lncRNA target analysis.
--run_enhancer_sampling
                      If you want to run the enhancer bootstrapping analysis, provide this flag.
--run_gtex_coloc      If you want to run COLOC analysis of your summary statistics against GTEx eQTLs from <span class="m">44</span> tissues <span class="o">(</span>requires summary statistics<span class="o">)</span>
--run_lncrna_correlation
                      If you want to analyze expression correlation of any lncRNAs identified by COLOC analysis <span class="o">(</span>--run_gtex_coloc flag<span class="o">)</span>
                      against all other RNAseq-based GTEx genes to find lncRNA targets.
              </pre></div>

              <p>For example, the full pipeline call for the schizophrenia analysis in the method manuscript would look like this using the parsed configuration file from the full annotation file, where the /path/to/XXX/ directories would be changed to wherever the files are on your server and XXX refers to the relevant path:</p>
              <div class="language-bash">
                <pre><span></span>$ <span class="nb">cd</span> /path/to/INFERNO_code/
$ python ./INFERNO.py --rsid_column <span class="m">2</span> --pos_column <span class="m">5</span> --pval_column <span class="m">9</span> --chr_column <span class="m">1</span> --allele1_column <span class="m">3</span> <span class="se">\</span>
  --allele2_column <span class="m">4</span> --maf_column <span class="m">15</span> --sig_mult <span class="m">10</span> --case_prop <span class="m">0</span>.2464882 --sample_size <span class="m">150064</span> <span class="se">\</span>
  --summary_file /path/to/SCZ_data/scz2.snp.results.1kg_annotations.txt --run_pval_expansion <span class="se">\</span>
  --run_enhancer_sampling --run_gtex_coloc --run_lncrna_correlation <span class="se">\</span>
  /path/to/SCZ_data/SCZ2_128_top_variants_INFERNO_input.no_chrX.tsv <span class="se">\</span>
  /path/to/annotations/full_INFERNO_annotations/INFERNO_annotation_config.cfg <span class="se">\</span>
  /path/to/output_folder/ SCZ2_128_top_variants
                </pre>
              </div>

              <p>The other way to run the pipeline is based on your cluster submission system using the 'module' approach to loading the relevant packages and dependencies. In this case, make sure that the system contains the following modules:</p>
              <ul>
                <li>python/2.7.9</li>
                <li>bedtools2</li>
                <li>R/3.2.3</li>
                <li>plink/1.90Beta</li>
              </ul>

              <p>Then, you can use the INFERNO.sh script to run the pipeline without requiring that the relevant dependencies are in your $PATH:</p>
              <div class="language-bash">
<pre>
$ <span class="nb">cd</span> /path/to/INFERNO_code/
$ ./INFERNO.sh --rsid_column <span class="m">2</span> --pos_column <span class="m">5</span> --pval_column <span class="m">9</span> --chr_column <span class="m">1</span> --allele1_column <span class="m">3</span> <span class="se">\</span>
  --allele2_column <span class="m">4</span> --maf_column <span class="m">15</span> --sig_mult <span class="m">10</span> --case_prop <span class="m">0</span>.2464882 --sample_size <span class="m">150064</span> <span class="se">\</span>
  --summary_file /path/to/SCZ_data/scz2.snp.results.1kg_annotations.txt --run_pval_expansion <span class="se">\</span>
  --run_enhancer_sampling --run_gtex_coloc --run_lncrna_correlation <span class="se">\</span>
  /path/to/SCZ_data/SCZ2_128_top_variants_INFERNO_input.no_chrX.tsv <span class="se">\</span>
  /path/to/annotations/full_INFERNO_annotations/INFERNO_annotation_config.cfg <span class="se">\</span>
  /path/to/output_folder/ SCZ2_128_top_variants
</pre>
            </div>

            <p>If you wanted to submit the same job in bsub, it would look like this:</p>
            <div class="language-bash">
<pre>$ <span class="nb">cd</span> /path/to/INFERNO_code/
$ bsub -J INFERNO_PGC_SCZ2_ld_pruned -o /path/to/output_folder/SCZ_analysis.o%J -e <span class="se">\</span>
  /path/to/output_folder/SCZ_analysis.e%J <span class="se">\</span>
  ./INFERNO.sh --rsid_column <span class="m">2</span> --pos_column <span class="m">5</span> --pval_column <span class="m">9</span> --chr_column <span class="m">1</span> --allele1_column <span class="m">3</span> <span class="se">\</span>
  --allele2_column <span class="m">4</span> --maf_column <span class="m">15</span> --sig_mult <span class="m">10</span> --case_prop <span class="m">0</span>.2464882 --sample_size <span class="m">150064</span> <span class="se">\</span>
  --summary_file /path/to/SCZ_data/scz2.snp.results.1kg_annotations.txt --run_pval_expansion <span class="se">\</span>
  --run_enhancer_sampling --run_gtex_coloc --run_lncrna_correlation <span class="se">\</span>
  /path/to/SCZ_data/SCZ2_128_top_variants_INFERNO_input.no_chrX.tsv <span class="se">\</span>
  /path/to/annotations/full_INFERNO_annotations/INFERNO_annotation_config.cfg <span class="se">\</span>
  /path/to/output_folder/ SCZ2_128_top_variants
</pre>
            </div>

            <p>If you want to run INFERNO as a cluster job but your cluster system doesn't use modules, you can go into the INFERNO.sh script and comment out lines 28-31 and then submit the INFERNO.sh script to your cluster system.</p>

      </div><!-- .text -->
      <div class="text">
        <h2>INFERNO output organization:</h2>
        <p>After the pipeline runs, several folders and files containing output tables and figures are generated, where outprefix is the last argument to the INFERNO script ("SCZ2_128_top_variants" in the above example):</p>
        <ul>
          <li>logs/, which contains the main annotation overlap log as well as any bsub output logs</li>
          <li>
            <p>P-value and LD expansion outputs:</p>
            <ul>
              <li>outprefix_pval_expanded_snps.txt: the full list of variants expanded by p-value, if this analysis is performed</li>
              <li>outprefix_pruning/: the results of the LD pruning analysis on the p-value expanded variant set</li>
              <li>ld_expansion/: the result of the LD expansion on either the LD pruned p-value expanded variants, or the input variants</li>
            </ul>
          </li>
          <li>
            <p>From the main INFERNO annotation overlaps (note that not all of these folders will be generated if you are missing some annotation data or skip some steps):</p>
            <ul>
              <li>parameters/: this folder contains the parameter and specific command run for the annotation overlaps</li>
              <li>closest_gene/: contains the closest genes to each variant</li>
              <li>unstranded_genomic_partition/: contains the results of the genomic partition analysis (i.e. annotation of variants overlapping promoters, exons, etc)</li>
              <li>fantom5_overlap/: variant overlap results with FANTOM5 enhancers</li>
              <li>closest_fantom5_enh/: contains the closest FANTOM5 enhancers to each variant</li>
              <li>correlation_enh_targets/: contains the correlation-based target genes of FANTOM5 enhancers with overlapping genetic variants</li>
              <li>gtex_eqtl_overlap/: the results of the direct eQTL overlap analysis (<strong>NOTE: do not use these results if you have summary statistics and can perform co-localization analysis</strong>)</li>
              <li>factorbook_overlap/: overlap with FactorBook TFBSs (these are essentially a subset of the HOMER TFBS annotations)</li>
              <li>homer_motif_overlap/: overlap with the more comprehensive set of HOMER motifs, including PWM calculations</li>
              <li>roadmap_chromhmm_states/: annotation of variants with Roadmap ChromHMM states across all tissues and cell types</li>
              <li>dashr_ncrna_loci_overlap/: contains overlaps with <a href="http://dashr2.lisanwanglab.org/">DASHR</a> small noncoding RNA loci</li>
              <li>targetscan_miRNA_overlap/: contains overlaps with predicted miRNA binding sites from TargetScan</li>
            </ul>
          </li>
          <li>
            <p>summaries/: this folder contains various summaries of annotation overlaps in each tissue class; these files are used for further analysis including enrichment sampling</p>
          </li>
          <li>
            <p>analysis_results/: this folder contains all the results from the R analysis script which performs summarization and visualization. This contains the following subfolders, each of which is further split into plots/ and tables/ folders:</p>
            <ul>
              <li>ld_stats/: visualizations of the LD expansion statistics for this analysis</li>
              <li>unstranded_genomic_partition/: visualizations of the genomic partition results</li>
              <li>fantom5_overlap/: visualizations of various characteristics of the FANTOM5 enhancer overlap results</li>
              <li>closest_fantom5_enhs/: analysis of closest FANTOM5 enhancers</li>
              <li>gtex_eqtl_overlap/: results from direct eQTL overlap with GTEx data. Again, <strong>do not use these results if you perform co-localization analysis</strong></li>
              <li>factorbook_overlap/: analysis of FactorBook TFBS overlaps</li>
              <li>homer_motif_overlap/: visualizations and analysis of HOMER TFBS results</li>
              <li>targetscan_miRNA_overlap/: analysis of miRNA binding site overlaps from TargetScan</li>
              <li>fantom5_roadmap_overlap/: integrative analysis of FANTOM5 and Roadmap enhancer overlaps</li>
              <li>fantom5_eqtl_chromHMM_overlap/: integrative analysis of FANTOM5 and Roadmap enhancer overlaps with direct GTEx eQTL overlaps</li>
            </ul>
          </li>
          <li>
            <p><span class="break">background_enh_sampling_match_and_expand/:</span> this folder contains the results of the enhancer sampling analysis</p>
            <ul>
              <li>plots/: contains many visualizations of the sampling results. One important
              characteristic is that the plots with '_collapsed_' in the name refer to the sampling
              analysis where multiple overlapping variants in one LD block only contribute one overlap
              count, and file names without it refer to the analysis where multiple variants always are
              counted individually, even if they are in the same LD block. The main visualization used
              for this analysis is <span class="break">outprefix_collapsed_annotation_by_tissue_bh_pval_heatmap_with_text.pdf</span>, which contains
              the collapsed analysis with multiple testing corrected p-values. <strong>INFERNO will report raw enrichment p-values, but these should not be used for measuring significance without
              multiple testing correction!</strong>
                <ul>
                  <li>input_sampling/: this contains diagnostic plots for the sampling against the input variants</li>
                </ul>
              </li>
              <li>ld_collapsed_split_tag_regions/: this folder contains plots for the sampling enrichment analysis using collapsed LD blocks for individual tag regions, with subfolders for each specific region</li>
              <li>split_tag_regions/: this folder contains plots for the sampling analysis without collapsing by LD blocks for individual tag regions</li>
              <li>samples/: this (very large) folder contains tables of the specific variants sampled
                against the input as well as expanded variants, and also contains files describing the
                specific LD blocks and tag regions for each sampled variant. These files are mostly just
                index tables for the R script to use and are not very informative to directly look at
              </li>
              <li>tables/: this contains easy to parse tables of the outputs of the various steps of the
                sampling analysis, including raw and corrected p-values for both the cross-tag-region
                analysis and the split tag region analyses as well as the LD-collapsed and non-collapsed
                analyses
              </li>
            </ul>
          </li>
          <li>
            <p>gtex_gwas_colocalization_analysis/: this folder contains the results of the eQTL
            co-localization analysis, if that step was run.</p>
            <ul>
              <li>plots/: this contains several diagnostic and summary plots for the analysis results. The
                main summary plots for the co-localization analysis are
                <span class="break">outprefix_ABF_and_motif_summary_barplot_0.5_prob_thresh.pdf</span> and
                <span class="break">outprefix_ABF_and_motif_nonzero_summary_barplot_0.5_prob_thresh_no_title.pdf</span>, which
                display summaries of the annotation overlaps compared with co-localization results across
                tag regions and different variant prioritization approaches.
                <ul>
                  <li>locuszoom_plots/: if the LOCUSZOOM variable for the path to your locuszoom executable
                    is defined in the config file, the colocalization analysis script will use it to
                    generate locusZoom plots of the GWAS and GTEx eQTL signals at each strongly
                    co-localized locus
                  </li>
                </ul>
              </li>
              <li>tables/: the top level of this folder contains tables of various characteristics of the
                co-localization results. These are all tab separated files and can be directly opened in
                Excel for ease of data exploration. The main file containing annotation overlaps and
                co-localization signals for all the ABF-expanded variants is
                <span class="break">outprefix_gtex_coloc_enh_motif_eqtl_info.0.5_thresh_expanded.txt</span>. <strong>Note: Excel will
                often complain if the file name is too long, and this happens quite often with these
                files as they are deep in a nested folder hierarchy. To get around this, just copy the
                full file and name it something like outprefix_COLOC_summary.txt, and then you should be
                able to open it in Excel.</strong>
                <ul>
                  <li>gtex_coloc/: this (very large) folder contains the full co-localization results
                    across tag regions (individual subfolders) and GTEx tissues. Typically you wouldn't
                    need to look at the specific files here because they are summarized by the analysis
                    script.
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li>
            <p>gtex_lncRNA_correlation_analysis/: if the correlation analysis to find lncRNA target genes was performed, this folder contains the results.</p>
            <ul>
              <li>full_correlation_tables/: this contains the full correlation results across all genes in
                the genome for each lncRNA identified in the co-localization analysis. These are the raw
                files and are summarized in a more easily digestible format in the other subfolders from
                this analysis.
              </li>
              <li>plots/: this contains summary and diagnostic plots across all lncRNAs as well as
                subfolders for each specific lncRNA containing correlation scatterplots
              </li>
              <li>tables/: this contains the lists of target genes identified by correlation threshold for
                each specific lncRNA as well as across all lncRNAs
                <span class="break">(all_lncRNA_genes_0.5_correlation_threshold.txt)</span>, as well as lists split by the GTEx
                tissue of the original lncRNA signal (in tissue_specific_gene_lists) and split by the
                tissue category of that original GTEx signal (class_specific_gene_lists). For all of
                these gene lists, if you want to do pathway analysis using
                <a href="http://webgestalt.org" target="_blank">WebGestalt</a>, use the 'genesymbol' option.
              </li>
            </ul>
          </li>
        </ul>
      </div><!-- .text -->
      <div class="text">
        <h2>Data sources and pre-processing steps:</h2>
        <h3>1,000 genomes data:</h3>
        <p>Files downloaded from <a href="http://csg.sph.umich.edu/abecasis/mach/download/1000G.2012-03-14.html" rel="nofollow" target="_blank"><span class="break">http://csg.sph.umich.edu/abecasis/mach/download/1000G.2012-03-14.html</span></a> for
            1,000 Genomes data split into 4 populations. Then, several scripts, all available in the
            data_preprocessing/ folder of the INFERNO code, are used to process these datasets. All the
            steps past the sorting command are for the enhancer sampling analysis. For example, the EUR
            population analysis:
        </p>
        <div class="language-bash">
<pre>$ <span class="nb">cd</span> /path/to/EUR_data/
$ tar -xzvf phase1_release_v3.20101123.snps_indels_svs.genotypes.refpanel.EUR.vcf.gz.tgz
<span class="c1">## sort the files</span>
$ mkdir sorted_files/
$ <span class="k">for</span> i in <span class="o">{</span><span class="m">1</span>..22<span class="o">}</span><span class="p">;</span> <span class="k">do</span> <span class="se">\</span>
  <span class="nb">echo</span> <span class="s2">"chr</span><span class="si">${</span><span class="nv">i</span><span class="si">}</span><span class="s2">"</span><span class="p">;</span> <span class="se">\</span>
  zcat chr<span class="si">${</span><span class="nv">i</span><span class="si">}</span>.phase1_release_v3.20101123.snps_indels_svs.genotypes.refpanel.EUR.vcf.gz <span class="p">|</span> grep <span class="s2">"#"</span> &gt; sorted_files/chr<span class="si">${</span><span class="nv">i</span><span class="si">}</span>.phase1_release_v3.20101123.snps_indels_svs.genotypes.refpanel.EUR.vcf<span class="p">;</span> <span class="se">\</span>
  zcat chr<span class="si">${</span><span class="nv">i</span><span class="si">}</span>.phase1_release_v3.20101123.snps_indels_svs.genotypes.refpanel.EUR.vcf.gz <span class="p">|</span> grep -v <span class="s2">"#"</span> <span class="p">|</span> sort -k2,2n &gt;&gt; sorted_files/chr<span class="si">${</span><span class="nv">i</span><span class="si">}</span>.phase1_release_v3.20101123.snps_indels_svs.genotypes.refpanel.EUR.vcf<span class="p">;</span> <span class="se">\</span>
<span class="k">done</span>
<span class="c1">## calculate minor allele frequencies</span>
$ <span class="nb">cd</span> /path/to/INFERNO_code/data_preprocessing/
$ ./calculate_maf.sh /path/to/EUR_data/ chr /path/to/EUR_data/MAF_info
<span class="c1">## calculate distance to nearest TSS</span>
$ ./calculate_tss_distance.sh /path/to/EUR_data/ chr /path/to/hg19_ref/hg19_refseq_tss.bed /path/to/bedtools_bin/ /path/to/EUR_data/dist_to_tss/
<span class="c1">## calculate pairwise LD (assumes bsub-based cluster system)</span>
$ ./bsub_calculate_pairwise_ld.sh /path/to/EUR_data/ chr /path/to/EUR_data/pairwise_ld/
<span class="c1">## can also just run this sequentially, which will take a while:</span>
$ ./calculate_pairwise_ld.sh /path/to/EUR_data/ chr /path/to/EUR_data/pairwise_ld/
<span class="c1">## use these LD pairs to precompute LD blocks (in this case, using 0.7 as the R^2 threshold):</span>
$ python compute_ld_sets.py <span class="m">0</span>.7 /path/to/EUR_data/pairwise_ld/ /path/to/EUR_data/precomputed_ld_sets/
<span class="c1">## finally, summarize all of these quantities for each variant, for sampling purposes:</span>
$ python summarize_ld_maf_dist_results.py --ld_threshold <span class="m">0</span>.7 /path/to/EUR_data/pairwise_ld/ /path/to/EUR_data/MAF_info/ <span class="se">\</span>
  /path/to/EUR_data/dist_to_tss/ /path/to/EUR_data/snp_maf_tss_ld_summary/
</pre>
        </div>

        <p>Then, to generate annotation overlaps and summaries for all of these variants, all variants from 1,000 genomes were run through the pipeline:</p>
        <div class="language-bash"><pre>$ <span class="nb">cd</span> /path/to/INFERNO/src/
$ python expand_and_annotate_snps.py --loglevel save --kg_pop EUR --skip_ld_expansion --unstranded_partition_dir <span class="se">\</span>
   ~/data/refgenomes/hg19/unstranded_partitions/utr_annotations/final_files/ <span class="se">\</span>
   --fantom5_dir ~/data/FANTOM5/Enhancers/facet_expressed_enhancers/sorted/ --enhancer_locus_window <span class="m">1000</span> --skip_closest_enh --skip_enh_summary --fantom5_correlation_file <span class="se">\</span>
   ~/data/FANTOM5/Enhancers/enhancer_tss_associations.bed --gtex_dir ~/data/GTEx/single_cell_sig_eqtls_v6/sorted/ --factorbook_file <span class="se">\</span>
   ~/data/factorbook/wgEncodeRegTfbsClusteredWithCellsV3.sorted.bed <span class="se">\</span>
   --roadmap_chromhmm_dir ~/data/roadmap/chromHMM/sorted/ --homer_motif_bed_file ~/data/HOMER_motifs/homer.sorted.KnownMotifs.hg19.bed --homer_motif_pwm_file <span class="se">\</span>
   ~/data/HOMER_motifs/custom.motifs <span class="se">\</span>
   --homer_motif_seq_file ~/data/HOMER_motifs/homer.sorted.KnownMotifs.hg19.sequence.txt ~/data/1000_genomes/phase1_release_v3/EUR/sorted_files/ <span class="se">\</span>
   ~/data/enhancer_snp_pipeline/input_data/no_indels_1kg_EUR_phase1_v3_snps.txt ~/data/enhancer_snp_pipeline/output/all_1kg_EUR_phase1_v3_snps_11_14_16/ all_1kg_EUR
$ <span class="nb">cd</span> ../analysis_scripts/
$ <span class="nb">time</span> ./large_scale_count_annotation_overlaps.sh -l <span class="m">1000</span> -f ~/data/FANTOM5/Enhancers/fantom5_classes.txt -g ~/data/GTEx/gtex_classes.txt -r ~/data/roadmap/roadmap_classes.txt <span class="se">\</span>
  ~/data/enhancer_snp_pipeline/output/all_1kg_EUR_phase1_v3_snps_11_14_16/ <span class="m">1</span>.0 <span class="m">0</span> all_1kg_EUR ~/data/enhancer_snp_pipeline/output/all_1kg_EUR_phase1_v3_snps_11_14_16/summaries/
$ <span class="nb">cd</span> ~/data/enhancer_snp_pipeline/output/all_1kg_EUR_phase1_v3_snps_11_14_16/summaries/
$ <span class="nb">time</span> cut -f1-3,6 enh_locus_snps_1.0_ld_0_dist.txt <span class="p">|</span> sort -u &gt; uniq_class_enh_locus_snps.txt<span class="p">;</span> <span class="se">\</span>
  <span class="nb">time</span> cut -f1-3,6 eqtl_snps_1.0_ld_0_dist.txt <span class="p">|</span> sort -u &gt; uniq_class_eqtl_snps.txt<span class="p">;</span> <span class="se">\</span>
  <span class="nb">time</span> cut -f1-3,6 roadmap_hmm_snps_1.0_ld_0_dist.txt <span class="p">|</span> sort -u &gt; uniq_class_roadmap_hmm_snps.txt
</pre>
        </div>

        <h3>Unstranded genomic partition:</h3>
        <p>Tables of UCSC genes were downloaded from the UCSC Table Browser, and only chr1-22, X and Y are
          used in INFERNO. The 5’ UTR exons and introns, 3’ UTR exons and introns, and exons and introns
          were extracted from the knownGene annotation for each protein-coding gene, and all overlapping
          exons were merged together. Promoter annotations were defined as 1,000bp upstream of the first
          exon in the transcript, either coding or in the UTR.  Variants were then assigned to mutually
          exclusive genomic element annotations using the hierarchy: 5’ UTR exon > 5’ UTR intron > 3’ UTR
          exon > 3’ UTR intron > promoter > mRNA exon > mRNA intron > repeat. A variant not overlapping
          with any class of elements above was classified as intergenic.
        </p>

        <h3>FANTOM5 enhancers:</h3>
        <p>Facet-level enhancer expression BED files were
          <a href="http://enhancer.binf.ku.dk/presets/facet_expressed_enhancers.tgz">downloaded</a> from
          <a href="http://enhancer.binf.ku.dk/presets/facet_expressed_enhancers.tgz" rel="nofollow"><span class="break">http://enhancer.binf.ku.dk/presets/facet_expressed_enhancers.tgz</span></a>, extracted, and sorted using
          the Unix sort tool with arguments ‘-k1,1V -k2,2n’. Specific commands are found in the file download_and_sort_FANTOM5_files.sh in the data_preprocessing/ directory.
        </p>

        <h3>Roadmap ChromHMM:</h3>
        <p>Roadmap 15-state ChromHMM BED files for the 5 core marks (H3K4me3, H3K4me1, H3K36me3, H3K27me3, H3K9me3) were
          <a href="http://egg2.wustl.edu/roadmap/data/byFileType/chromhmmSegmentations/ChmmModels/coreMarks/jointModel/final/all.mnemonics.bedFiles.tgz">downloaded</a>
          from <a href="http://egg2.wustl.edu/roadmap/data/byFileType/chromhmmSegmentations/ChmmModels/coreMarks/jointModel/final/all.mnemonics.bedFiles.tgz" rel="nofollow"><span class="break">http://egg2.wustl.edu/roadmap/data/byFileType/chromhmmSegmentations/ChmmModels/coreMarks/jointModel/final/all.mnemonics.bedFiles.tgz</span></a>,
          extracted, and sorted similarly to the FANTOM5 annotations; commands are available in the file roadmap_chromhmm_download_and_sort.sh.
        </p>

        <h3>HOMER TFBSs:</h3>
        <p>The set of HOMER TFBS annotations was
          <a href="http://homer.ucsd.edu/homer/data/motifs/homer.KnownMotifs.hg19.bed.gz">downloaded</a> from
          <a href="http://homer.ucsd.edu/homer/data/motifs/homer.KnownMotifs.hg19.bed.gz" rel="nofollow"><span class="break">http://homer.ucsd.edu/homer/data/motifs/homer.KnownMotifs.hg19.bed.gz</span></a> and PWM annotations were
          <a href="http://homer.ucsd.edu/homer/custom.motifs">downloaded</a> from
          <a href="http://homer.ucsd.edu/homer/custom.motifs" rel="nofollow"><span class="break">http://homer.ucsd.edu/homer/custom.motifs</span></a>. The BED file was extracted and sorted using the same
          approach as the FANTOM5 and Roadmap data, and the getfasta tool from the bedtools suite was used to generate sequences for each BED interval in order to calculate the ∆PWM score, using
          the hg19 fasta file downloaded from the UCSC table browser. The specific command used is available in HOMER_seq_generating_cmds.sh in the data_preprocessing/ folder.
        </p>

        <h3>GTEx data:</h3>
        <p>For direct eQTL overlap analysis, files were downloaded and sorted as follows:</p>
        <div class="language-bash"><pre>$ <span class="nb">cd</span> /path/to/eQTL_dir/
$ wget http://www.gtexportal.org/static/datasets/gtex_analysis_v6p/single_tissue_eqtl_data/GTEx_Analysis_V6p_eQTLs.tar.gz
$ tar -xzvf GTEx_Analysis_V6p_eQTLs.tar.gz
$ mkdir sorted
$ <span class="k">for</span> f in *.snpgenes<span class="p">;</span> <span class="k">do</span> <span class="se">\</span>
    <span class="nv">FNAME</span><span class="o">=</span><span class="sb">`</span>basename <span class="nv">$f</span><span class="sb">`</span><span class="p">;</span> <span class="se">\</span>
    <span class="nb">echo</span> <span class="s2">"Sorting file </span><span class="si">${</span><span class="nv">FNAME</span><span class="si">}</span><span class="s2">"</span><span class="p">;</span> <span class="se">\</span>
    sort -k14,14n -k15,15n <span class="nv">$f</span> &gt; sorted/<span class="nv">$FNAME</span><span class="p">;</span> <span class="se">\</span>
    rm <span class="nv">$f</span><span class="p">;</span> <span class="se">\</span>
<span class="k">done</span>
</pre>
        </div>
        <p>For colocalization analysis, the set of all GTEx eQTL tests (not just significant ones) was <a href="http://www.gtexportal.org/static/datasets/gtex_analysis_v6p/single_tissue_eqtl_data/GTEx_Analysis_v6p_all-associations.tar">downloaded</a>
          from <a href="http://www.gtexportal.org/static/datasets/gtex_analysis_v6p/single_tissue_eqtl_data/GTEx_Analysis_v6p_all-associations.tar" rel="nofollow"><span class="break">http://www.gtexportal.org/static/datasets/gtex_analysis_v6p/single_tissue_eqtl_data/GTEx_Analysis_v6p_all-associations.tar</span></a>,
          extracted, and sorted. Specific commands are available in the gtex_download_and_sort_full_v6p_data.sh in the data preprocessing/ folder.
        </p>
        <p>For the lncRNA correlation analysis, RNA-seq read per kilobase per million (RPKM) values across all 44 tissues were
          <a href="http://gtexportal.org/static/datasets/gtex_analysis_v6p/rna_seq_data/GTEx_Analysis_v6p_RNA-seq_RNA-SeQCv1.1.8_gene_rpkm.gct.gz">downloaded</a>
          from <a href="http://gtexportal.org/static/datasets/gtex_analysis_v6p/rna_seq_data/GTEx_Analysis_v6p_RNA-seq_RNA-SeQCv1.1.8_gene_rpkm.gct.gz" rel="nofollow"><span class="break">http://gtexportal.org/static/datasets/gtex_analysis_v6p/rna_seq_data/GTEx_Analysis_v6p_RNA-seq_RNA-SeQCv1.1.8_gene_rpkm.gct.gz</span></a>.
        </p>
        <p>Then, a few pre-processing scripts were used to parse these for use in the correlation script:</p>
        <div class="language-bash"><pre>$ <span class="nb">cd</span> /path/to/INFERNO/data_preprocessing/
$ python find_ensembl_genes_by_chr.py /path/to/hg19_reference/ensembl_hg19_genes.txt /path/to/RNAseq_output_dir/
$ ./all_chr_gtex_expression_filter.sh /path/to/RNAseq_output_dir/ /path/to/GTEx_Analysis_v6_RNA-seq_RNA-SeQCv1.1.8_gene_rpkm.gct.gz
</pre>
        </div>
        <p>To identify lncRNA eQTL targets, the GENCODE lncRNA annotations were <a href="ftp://ftp.sanger.ac.uk/pub/gencode/Gencode_human/release_19/gencode.v19.long_noncoding_RNAs.gtf.gz">downloaded</a>
          and extracted from ftp://<a href="http://ftp.sanger.ac.uk/pub/gencode/Gencode_human/release_19/gencode.v19.long_noncoding_RNAs.gtf.gz" rel="nofollow"><span class="break">ftp.sanger.ac.uk/pub/gencode/Gencode_human/release_19/gencode.v19.long_noncoding_RNAs.gtf.gz</span></a>.
        </p>
        <p>Finally, the sample sizes for each GTEx tissue were also <a href="http://www.gtexportal.org/home/tissueSummaryPage#sampleCountsPerTissue" target="_blank">downloaded</a> as a csv
          from <a href="http://www.gtexportal.org/home/tissueSummaryPage#sampleCountsPerTissue" rel="nofollow" target="_blank"><span class="break">http://www.gtexportal.org/home/tissueSummaryPage#sampleCountsPerTissue</span></a>.
        </p>

      </div><!-- .text -->

    </main><!-- .main-area -->
  </div><!-- .content -->

  <?php include "components/footer.php"; ?>
