(function($){
  // Front page textarea placeholder
  $(document).on('input', '#text-area', function () {
      if ($('#text-area').val()) {
          $('#placeholder').hide();
      } else {
          $('#placeholder').show();
      }
  });

  // DataSource SWAPPER
$('#plot-names a.swap').click(function(event){
    event.preventDefault();//stop browser to take action for clicked anchor

    // hide currently displaying content
    $('#plot-names a').removeClass('active');
    $('.plot-result div#plot-1').hide();
    $('.plot-result div.swap').hide();

    //display selected content
    $(this).addClass('active');
    var target_tab_selector = $(this).attr('href');
    target_tab_selector = 'div' + target_tab_selector;
    $(target_tab_selector).fadeIn('fast');
});

})(jQuery);
