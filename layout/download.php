<?php include "components/header.php"; ?>
  <div class="content">
    <main class="main-area">
      <h1>Download INFERNO</h1>
      <div class="text center">
        <p class="download">To obtain the full INFERNO pipeline for your own analyses,<br> see the <a href="https://bitbucket.org/wanglab-upenn/INFERNO" target="_blank">bitbucket repo.</a></p>
      </div><!-- .text -->
    </main><!-- .main-area -->
  </div><!-- .content -->

  <?php include "components/footer.php"; ?>
